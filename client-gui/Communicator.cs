﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net.Sockets;
using System.Net;

namespace client_gui
{
    class Communicator
    {
        private const int port = 7777;
        private NetworkStream clientStream;

        public Communicator()
        {
            //Making a new tcp client
            TcpClient client = new TcpClient();
            //Connecting to server
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);
            client.Connect(serverEndPoint);

            //making a stream to talk to server and send/get requests/responses
            this.clientStream = client.GetStream();

            //setting read timeout
            this.clientStream.ReadTimeout = 3000;
        }

        public Communicator(NetworkStream clientStream)
        {
            this.clientStream = clientStream;
        }
        public NetworkStream GetStream()
        {
            return this.clientStream;
        }

        public void RequestToServer(string message)
        {
            try 
            {
                byte[] buffer = new ASCIIEncoding().GetBytes(message);
                clientStream.Write(buffer, 0, buffer.Length);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public string ResponseFromServer()
        {
            if (this.clientStream.CanRead)
            {
                byte[] buffer = new byte[4096];

                try
                {
                    int bytesRead = clientStream.Read(buffer, 0, buffer.Length);

                    return System.Text.Encoding.UTF8.GetString(buffer);
                }
                catch (Exception){ }
            }
            return "";
        }
    }
}
