﻿
namespace client_gui
{
    partial class BestScoresForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.stat3 = new System.Windows.Forms.Label();
            this.stat2 = new System.Windows.Forms.Label();
            this.stat1 = new System.Windows.Forms.Label();
            this.backBtn = new System.Windows.Forms.Button();
            this.MainTitle = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // stat3
            // 
            this.stat3.AutoSize = true;
            this.stat3.Font = new System.Drawing.Font("Segoe UI Emoji", 32.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stat3.Location = new System.Drawing.Point(330, 282);
            this.stat3.Name = "stat3";
            this.stat3.Size = new System.Drawing.Size(0, 57);
            this.stat3.TabIndex = 39;
            // 
            // stat2
            // 
            this.stat2.AutoSize = true;
            this.stat2.Font = new System.Drawing.Font("Segoe UI Emoji", 32.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stat2.Location = new System.Drawing.Point(330, 218);
            this.stat2.Name = "stat2";
            this.stat2.Size = new System.Drawing.Size(0, 57);
            this.stat2.TabIndex = 38;
            // 
            // stat1
            // 
            this.stat1.AutoSize = true;
            this.stat1.Font = new System.Drawing.Font("Segoe UI Emoji", 32.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stat1.Location = new System.Drawing.Point(330, 154);
            this.stat1.Name = "stat1";
            this.stat1.Size = new System.Drawing.Size(0, 57);
            this.stat1.TabIndex = 37;
            // 
            // backBtn
            // 
            this.backBtn.BackColor = System.Drawing.Color.PaleTurquoise;
            this.backBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.backBtn.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backBtn.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.backBtn.Location = new System.Drawing.Point(686, 384);
            this.backBtn.Name = "backBtn";
            this.backBtn.Size = new System.Drawing.Size(102, 46);
            this.backBtn.TabIndex = 36;
            this.backBtn.Text = "back";
            this.backBtn.UseVisualStyleBackColor = false;
            this.backBtn.Click += new System.EventHandler(this.backBtn_Click);
            // 
            // MainTitle
            // 
            this.MainTitle.AutoSize = true;
            this.MainTitle.Font = new System.Drawing.Font("Ravie", 48F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainTitle.Location = new System.Drawing.Point(118, 34);
            this.MainTitle.Name = "MainTitle";
            this.MainTitle.Size = new System.Drawing.Size(556, 86);
            this.MainTitle.TabIndex = 35;
            this.MainTitle.Text = "Best Scores";
            // 
            // name
            // 
            this.name.AutoSize = true;
            this.name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.name.Location = new System.Drawing.Point(12, 9);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(37, 13);
            this.name.TabIndex = 40;
            this.name.Text = "name";
            // 
            // BestScoresForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.name);
            this.Controls.Add(this.stat3);
            this.Controls.Add(this.stat2);
            this.Controls.Add(this.stat1);
            this.Controls.Add(this.backBtn);
            this.Controls.Add(this.MainTitle);
            this.Name = "BestScoresForm";
            this.Text = "Trivia - Best Scores";
            this.Load += new System.EventHandler(this.BestScoresForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label stat3;
        private System.Windows.Forms.Label stat2;
        private System.Windows.Forms.Label stat1;
        private System.Windows.Forms.Button backBtn;
        private System.Windows.Forms.Label MainTitle;
        private System.Windows.Forms.Label name;
    }
}