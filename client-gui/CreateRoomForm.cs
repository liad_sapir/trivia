﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net.Sockets;
using System.Net;

namespace client_gui
{
    public partial class CreateRoomForm : Form
    {
        private Client _client;
        public CreateRoomForm(NetworkStream clientStream, string name)
        {
            InitializeComponent();
            this._client = new Client(clientStream);
            this.name.Text = name;
        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            GoBack();
        }

        private void GoBack()
        {
            //hiding form
            this.Hide();

            //moving to next form
            Form wnd = new LoggedOptionsForm(this._client.GetStream(), this.name.Text);
            wnd.ShowDialog();

            //closing form
            this.Close();
        }

        private void sendBtn_Click(object sender, EventArgs e)
        {
            if(this._client.CreateRoom(roomNameTextBox.Text, numPlayersTextBox.Text, numQTextBox.Text, ansTimeoutTextBox.Text))
            {
                //hiding form
                this.Hide();

                //moving to next form
                Form wnd = new ConnectedToRoomForm(true, roomNameTextBox.Text, this._client.GetStream(), this.name.Text);
                wnd.ShowDialog();

                //closing form
                this.Close();
            }
            else
            {
                MessageBox.Show("A room already exists with the given name!");
            }
        }
    }
}
