﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net.Sockets;
using System.Net;

namespace client_gui
{
    public partial class BestScoresForm : Form
    {
        private Client _client;
        public BestScoresForm(NetworkStream clientStream, string name)
        {
            InitializeComponent();
            this._client = new Client(clientStream);
            this.name.Text = name;
        }
        private void backBtn_Click(object sender, EventArgs e)
        {
            //hiding form
            this.Hide();

            //moving to next form
            Form wnd = new LoggedOptionsForm(this._client.GetStream(), this.name.Text);
            wnd.ShowDialog();

            //closing form
            this.Close();
        }

        private void BestScoresForm_Load(object sender, EventArgs e)
        {
            string[] stats = this._client.GetHighScores();
            if (stats != null)
            {
                if(stats.Length == 1)stat1.Text = stats[0];
                if (stats.Length == 2) stat2.Text = stats[1];
                if (stats.Length == 3) stat3.Text = stats[2];
            }
        }
    }
}
