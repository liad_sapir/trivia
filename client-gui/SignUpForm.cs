﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net.Sockets;
using System.Net;

namespace client_gui
{
    public partial class SignUpForm : Form
    {
        private Client _client;

        public SignUpForm(NetworkStream clientStream)
        {
            InitializeComponent();
            this._client = new Client(clientStream);
        }

        private void SignUpBtn_Clicked(object sender, EventArgs e)
        {
            if (_client.SignUp(userNameSignTextBox.Text, passwordSignTextBox.Text, emailSignTextBox.Text))
            {
                GoBack();
            }
            else
            {
                //message for wrong details
                DetailsLabel.Visible = true;
            }
        }

        private void BackBtn_Clicked(object sender, EventArgs e)
        {
            GoBack();
        }

        private void GoBack()
        {
            //hiding form
            this.Hide();

            //moving to next form
            Form wnd = new MainForm(this._client.GetStream());
            wnd.ShowDialog();

            //closing form
            this.Close();
        }

        private void SignUpForm_Load(object sender, EventArgs e)
        {

        }
    }
}
