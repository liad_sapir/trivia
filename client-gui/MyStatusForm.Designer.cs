﻿
namespace client_gui
{
    partial class MyStatusForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.stat1 = new System.Windows.Forms.Label();
            this.backBtn = new System.Windows.Forms.Button();
            this.MainTitle = new System.Windows.Forms.Label();
            this.stat2 = new System.Windows.Forms.Label();
            this.stat3 = new System.Windows.Forms.Label();
            this.stat4 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // stat1
            // 
            this.stat1.AutoSize = true;
            this.stat1.Font = new System.Drawing.Font("Segoe UI Emoji", 32.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stat1.Location = new System.Drawing.Point(578, 131);
            this.stat1.Name = "stat1";
            this.stat1.Size = new System.Drawing.Size(49, 57);
            this.stat1.TabIndex = 31;
            this.stat1.Text = "0";
            // 
            // backBtn
            // 
            this.backBtn.BackColor = System.Drawing.Color.PaleTurquoise;
            this.backBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.backBtn.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backBtn.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.backBtn.Location = new System.Drawing.Point(686, 392);
            this.backBtn.Name = "backBtn";
            this.backBtn.Size = new System.Drawing.Size(102, 46);
            this.backBtn.TabIndex = 29;
            this.backBtn.Text = "back";
            this.backBtn.UseVisualStyleBackColor = false;
            this.backBtn.Click += new System.EventHandler(this.backBtn_Click);
            // 
            // MainTitle
            // 
            this.MainTitle.AutoSize = true;
            this.MainTitle.Font = new System.Drawing.Font("Ravie", 48F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainTitle.Location = new System.Drawing.Point(-12, 28);
            this.MainTitle.Name = "MainTitle";
            this.MainTitle.Size = new System.Drawing.Size(825, 86);
            this.MainTitle.TabIndex = 27;
            this.MainTitle.Text = "My Performances";
            // 
            // stat2
            // 
            this.stat2.AutoSize = true;
            this.stat2.Font = new System.Drawing.Font("Segoe UI Emoji", 32.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stat2.Location = new System.Drawing.Point(578, 195);
            this.stat2.Name = "stat2";
            this.stat2.Size = new System.Drawing.Size(49, 57);
            this.stat2.TabIndex = 32;
            this.stat2.Text = "0";
            // 
            // stat3
            // 
            this.stat3.AutoSize = true;
            this.stat3.Font = new System.Drawing.Font("Segoe UI Emoji", 32.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stat3.Location = new System.Drawing.Point(578, 259);
            this.stat3.Name = "stat3";
            this.stat3.Size = new System.Drawing.Size(49, 57);
            this.stat3.TabIndex = 33;
            this.stat3.Text = "0";
            // 
            // stat4
            // 
            this.stat4.AutoSize = true;
            this.stat4.Font = new System.Drawing.Font("Segoe UI Emoji", 32.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stat4.Location = new System.Drawing.Point(578, 323);
            this.stat4.Name = "stat4";
            this.stat4.Size = new System.Drawing.Size(49, 57);
            this.stat4.TabIndex = 34;
            this.stat4.Text = "0";
            // 
            // name
            // 
            this.name.AutoSize = true;
            this.name.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.name.Location = new System.Drawing.Point(12, 9);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(49, 18);
            this.name.TabIndex = 35;
            this.name.Text = "name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Emoji", 32.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(106, 131);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(396, 57);
            this.label1.TabIndex = 36;
            this.label1.Text = "Number of Games:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Emoji", 32.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(106, 195);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(311, 57);
            this.label2.TabIndex = 37;
            this.label2.Text = "Total Answers:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Emoji", 32.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(106, 259);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(463, 57);
            this.label3.TabIndex = 38;
            this.label3.Text = "Average Answer Time:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Emoji", 32.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(106, 323);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(357, 57);
            this.label4.TabIndex = 39;
            this.label4.Text = "Correct Answers:";
            // 
            // MyStatusForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.name);
            this.Controls.Add(this.stat4);
            this.Controls.Add(this.stat3);
            this.Controls.Add(this.stat2);
            this.Controls.Add(this.stat1);
            this.Controls.Add(this.backBtn);
            this.Controls.Add(this.MainTitle);
            this.Name = "MyStatusForm";
            this.Text = "Trivia - Status";
            this.Load += new System.EventHandler(this.MyStatusForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label stat1;
        private System.Windows.Forms.Button backBtn;
        private System.Windows.Forms.Label MainTitle;
        private System.Windows.Forms.Label stat2;
        private System.Windows.Forms.Label stat3;
        private System.Windows.Forms.Label stat4;
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}