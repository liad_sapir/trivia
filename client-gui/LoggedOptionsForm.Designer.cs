﻿
namespace client_gui
{
    partial class LoggedOptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainTitle = new System.Windows.Forms.Label();
            this.joinRoomBtn = new System.Windows.Forms.Button();
            this.myStatusBtn = new System.Windows.Forms.Button();
            this.createRoomBtn = new System.Windows.Forms.Button();
            this.bestScoresBtn = new System.Windows.Forms.Button();
            this.backBtn = new System.Windows.Forms.Button();
            this.name = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // MainTitle
            // 
            this.MainTitle.AutoSize = true;
            this.MainTitle.Font = new System.Drawing.Font("Goudy Stout", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainTitle.Location = new System.Drawing.Point(12, 7);
            this.MainTitle.Name = "MainTitle";
            this.MainTitle.Size = new System.Drawing.Size(749, 132);
            this.MainTitle.TabIndex = 7;
            this.MainTitle.Text = "Trivia";
            // 
            // joinRoomBtn
            // 
            this.joinRoomBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.joinRoomBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.joinRoomBtn.Font = new System.Drawing.Font("Matura MT Script Capitals", 24F, System.Drawing.FontStyle.Bold);
            this.joinRoomBtn.ForeColor = System.Drawing.Color.Blue;
            this.joinRoomBtn.Location = new System.Drawing.Point(30, 159);
            this.joinRoomBtn.Name = "joinRoomBtn";
            this.joinRoomBtn.Size = new System.Drawing.Size(338, 73);
            this.joinRoomBtn.TabIndex = 6;
            this.joinRoomBtn.Text = "Join Room";
            this.joinRoomBtn.UseVisualStyleBackColor = false;
            this.joinRoomBtn.Click += new System.EventHandler(this.joinRoomBtn_Click);
            // 
            // myStatusBtn
            // 
            this.myStatusBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.myStatusBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.myStatusBtn.Font = new System.Drawing.Font("Matura MT Script Capitals", 24F, System.Drawing.FontStyle.Bold);
            this.myStatusBtn.ForeColor = System.Drawing.Color.Magenta;
            this.myStatusBtn.Location = new System.Drawing.Point(30, 255);
            this.myStatusBtn.Name = "myStatusBtn";
            this.myStatusBtn.Size = new System.Drawing.Size(338, 73);
            this.myStatusBtn.TabIndex = 8;
            this.myStatusBtn.Text = "My Status";
            this.myStatusBtn.UseVisualStyleBackColor = false;
            this.myStatusBtn.Click += new System.EventHandler(this.myStatusBtn_Click);
            // 
            // createRoomBtn
            // 
            this.createRoomBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.createRoomBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.createRoomBtn.Font = new System.Drawing.Font("Matura MT Script Capitals", 24F, System.Drawing.FontStyle.Bold);
            this.createRoomBtn.ForeColor = System.Drawing.Color.Blue;
            this.createRoomBtn.Location = new System.Drawing.Point(417, 159);
            this.createRoomBtn.Name = "createRoomBtn";
            this.createRoomBtn.Size = new System.Drawing.Size(338, 73);
            this.createRoomBtn.TabIndex = 9;
            this.createRoomBtn.Text = "Create Room";
            this.createRoomBtn.UseVisualStyleBackColor = false;
            this.createRoomBtn.Click += new System.EventHandler(this.createRoomBtn_Click);
            // 
            // bestScoresBtn
            // 
            this.bestScoresBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bestScoresBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bestScoresBtn.Font = new System.Drawing.Font("Matura MT Script Capitals", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bestScoresBtn.ForeColor = System.Drawing.Color.Magenta;
            this.bestScoresBtn.Location = new System.Drawing.Point(417, 255);
            this.bestScoresBtn.Name = "bestScoresBtn";
            this.bestScoresBtn.Size = new System.Drawing.Size(338, 73);
            this.bestScoresBtn.TabIndex = 10;
            this.bestScoresBtn.Text = "Best Scores";
            this.bestScoresBtn.UseVisualStyleBackColor = false;
            this.bestScoresBtn.Click += new System.EventHandler(this.bestScoresBtn_Click);
            // 
            // backBtn
            // 
            this.backBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.backBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.backBtn.Font = new System.Drawing.Font("Mistral", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.backBtn.Location = new System.Drawing.Point(234, 370);
            this.backBtn.Name = "backBtn";
            this.backBtn.Size = new System.Drawing.Size(321, 46);
            this.backBtn.TabIndex = 11;
            this.backBtn.Text = "Back And Sign Out";
            this.backBtn.UseVisualStyleBackColor = false;
            this.backBtn.Click += new System.EventHandler(this.backBtn_Click);
            // 
            // name
            // 
            this.name.AutoSize = true;
            this.name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.name.Location = new System.Drawing.Point(13, 7);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(37, 13);
            this.name.TabIndex = 12;
            this.name.Text = "name";
            // 
            // LoggedOptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.name);
            this.Controls.Add(this.backBtn);
            this.Controls.Add(this.bestScoresBtn);
            this.Controls.Add(this.createRoomBtn);
            this.Controls.Add(this.myStatusBtn);
            this.Controls.Add(this.MainTitle);
            this.Controls.Add(this.joinRoomBtn);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Name = "LoggedOptionsForm";
            this.Text = "Trivia";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label MainTitle;
        private System.Windows.Forms.Button joinRoomBtn;
        private System.Windows.Forms.Button myStatusBtn;
        private System.Windows.Forms.Button createRoomBtn;
        private System.Windows.Forms.Button bestScoresBtn;
        private System.Windows.Forms.Button backBtn;
        private System.Windows.Forms.Label name;
    }
}