﻿
namespace client_gui
{
    partial class SignUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainTitle = new System.Windows.Forms.Label();
            this.passwordSignTextBox = new System.Windows.Forms.TextBox();
            this.userNameSignTextBox = new System.Windows.Forms.TextBox();
            this.signUpBtn = new System.Windows.Forms.Button();
            this.emailSignTextBox = new System.Windows.Forms.TextBox();
            this.backBtn = new System.Windows.Forms.Button();
            this.DetailsLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // MainTitle
            // 
            this.MainTitle.AutoSize = true;
            this.MainTitle.Font = new System.Drawing.Font("Ravie", 72F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainTitle.Location = new System.Drawing.Point(69, 9);
            this.MainTitle.Name = "MainTitle";
            this.MainTitle.Size = new System.Drawing.Size(657, 129);
            this.MainTitle.TabIndex = 10;
            this.MainTitle.Text = "Welcome!";
            // 
            // passwordSignTextBox
            // 
            this.passwordSignTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.passwordSignTextBox.Font = new System.Drawing.Font("Modern No. 20", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordSignTextBox.Location = new System.Drawing.Point(202, 234);
            this.passwordSignTextBox.Name = "passwordSignTextBox";
            this.passwordSignTextBox.Size = new System.Drawing.Size(205, 36);
            this.passwordSignTextBox.TabIndex = 9;
            this.passwordSignTextBox.Text = "1234";
            // 
            // userNameSignTextBox
            // 
            this.userNameSignTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.userNameSignTextBox.Font = new System.Drawing.Font("Modern No. 20", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userNameSignTextBox.Location = new System.Drawing.Point(202, 162);
            this.userNameSignTextBox.Name = "userNameSignTextBox";
            this.userNameSignTextBox.Size = new System.Drawing.Size(205, 36);
            this.userNameSignTextBox.TabIndex = 8;
            this.userNameSignTextBox.Text = "user";
            // 
            // signUpBtn
            // 
            this.signUpBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.signUpBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.signUpBtn.Font = new System.Drawing.Font("Matura MT Script Capitals", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.signUpBtn.Location = new System.Drawing.Point(478, 196);
            this.signUpBtn.Name = "signUpBtn";
            this.signUpBtn.Size = new System.Drawing.Size(261, 108);
            this.signUpBtn.TabIndex = 6;
            this.signUpBtn.Text = "Sign Up";
            this.signUpBtn.UseVisualStyleBackColor = false;
            this.signUpBtn.TextChanged += new System.EventHandler(this.SignUpBtn_Clicked);
            this.signUpBtn.Click += new System.EventHandler(this.SignUpBtn_Clicked);
            // 
            // emailSignTextBox
            // 
            this.emailSignTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.emailSignTextBox.Font = new System.Drawing.Font("Modern No. 20", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailSignTextBox.Location = new System.Drawing.Point(202, 309);
            this.emailSignTextBox.Name = "emailSignTextBox";
            this.emailSignTextBox.Size = new System.Drawing.Size(205, 36);
            this.emailSignTextBox.TabIndex = 11;
            this.emailSignTextBox.Text = "user@gmail.com";
            // 
            // backBtn
            // 
            this.backBtn.BackColor = System.Drawing.Color.PaleTurquoise;
            this.backBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.backBtn.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backBtn.Location = new System.Drawing.Point(478, 359);
            this.backBtn.Name = "backBtn";
            this.backBtn.Size = new System.Drawing.Size(228, 46);
            this.backBtn.TabIndex = 12;
            this.backBtn.Text = "back";
            this.backBtn.UseVisualStyleBackColor = false;
            this.backBtn.Click += new System.EventHandler(this.BackBtn_Clicked);
            // 
            // DetailsLabel
            // 
            this.DetailsLabel.AutoSize = true;
            this.DetailsLabel.Font = new System.Drawing.Font("Miriam Fixed", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DetailsLabel.ForeColor = System.Drawing.Color.Red;
            this.DetailsLabel.Location = new System.Drawing.Point(454, 307);
            this.DetailsLabel.Name = "DetailsLabel";
            this.DetailsLabel.Size = new System.Drawing.Size(308, 20);
            this.DetailsLabel.TabIndex = 13;
            this.DetailsLabel.Text = "Username already exists";
            this.DetailsLabel.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 20.25F);
            this.label1.Location = new System.Drawing.Point(72, 165);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 29);
            this.label1.TabIndex = 14;
            this.label1.Text = "username:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Modern No. 20", 20.25F);
            this.label2.Location = new System.Drawing.Point(72, 237);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 29);
            this.label2.TabIndex = 15;
            this.label2.Text = "password:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Modern No. 20", 20.25F);
            this.label3.Location = new System.Drawing.Point(72, 312);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 29);
            this.label3.TabIndex = 16;
            this.label3.Text = "email:";
            // 
            // SignUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DetailsLabel);
            this.Controls.Add(this.backBtn);
            this.Controls.Add(this.emailSignTextBox);
            this.Controls.Add(this.MainTitle);
            this.Controls.Add(this.passwordSignTextBox);
            this.Controls.Add(this.userNameSignTextBox);
            this.Controls.Add(this.signUpBtn);
            this.Name = "SignUpForm";
            this.Text = "Trivia - Sign Up";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label MainTitle;
        private System.Windows.Forms.TextBox passwordSignTextBox;
        private System.Windows.Forms.TextBox userNameSignTextBox;
        private System.Windows.Forms.Button signUpBtn;
        private System.Windows.Forms.TextBox emailSignTextBox;
        private System.Windows.Forms.Button backBtn;
        private System.Windows.Forms.Label DetailsLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}