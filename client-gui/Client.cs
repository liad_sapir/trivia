﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

using System.Net.Sockets;
using System.Net;

namespace client_gui
{
    class Client
    {
        private Communicator communicator;
        private enum CodesLogin
        {
            ERROR_CODE = '0',
            LOGIN_CODE = '1',
            SIGNUP_CODE = '2',
            LOGOUT_CODE = '3'
        };

        private enum RoomRelatedCodes
        {
            GET_ROOMS_CODE = '4',
            GET_PLAYERS_IN_ROOM_CODE = '5',
            JOIN_ROOM_CODE = '6',
            CREATE_ROOM_CODE = '7',
        };

        private enum PersonalDataCodes
        {
            GET_PERSONAL_STATS_CODE = '8',
            GET_HIGH_SCORES_CODE = '9',
        };

        private enum RoomAdminCodes
        {
            CLOSE_ROOM_CODE = 'A',
            START_GAME_CODE = 'B',
            GET_ROOM_STATE_CODE = 'C',
        };

        private enum RoomMemberCode
        {
            LEAVE_ROOM_CODE = 'D'
        };

        public Client()
        {
            this.communicator = new Communicator();
        }

        public Client(NetworkStream clientStream)
        {
            this.communicator = new Communicator(clientStream);
        }

        public NetworkStream GetStream()
        {
            return this.communicator.GetStream();
        }

        public bool Login(string username, string password)
        {
            string message = "{\"username\": \"" + username + "\", \"password\": \"" + password + "\"}";

            this.communicator.RequestToServer((char)CodesLogin.LOGIN_CODE + String.Format("{0:0000}", message.Length) + message);
            
            string response = this.communicator.ResponseFromServer();

            return response.Contains("status: 1");
        }

        public bool SignUp(string username, string password, string email)
        {
            string message = "{\"username\": \"" + username + "\", \"password\": \"" + password + "\", \"email\": \"" + email + "\"}";

            this.communicator.RequestToServer((char)CodesLogin.SIGNUP_CODE + String.Format("{0:0000}", message.Length) + message);

            string response = this.communicator.ResponseFromServer();

            return response.Contains("status: 1");
        }

        public bool Logout()
        {
            this.communicator.RequestToServer(((char)CodesLogin.LOGOUT_CODE).ToString());

            string response = this.communicator.ResponseFromServer();

            return response.Contains("status: 1");
        }

        public string[] GetRooms()
        {
            this.communicator.RequestToServer(((char)RoomRelatedCodes.GET_ROOMS_CODE).ToString());

            string response = this.communicator.ResponseFromServer();

            if (response.Contains("Rooms")) return response.Split(':')[1].Remove(response.Split(':')[1].Length - 1).Split('"')[1].Split(',');

            return null;
        }

        public string[] GetPlayersInRoom(string roomName)
        {
            string message = "{\"roomName\": \"" + roomName + "\"}";

            this.communicator.RequestToServer((char)RoomRelatedCodes.GET_PLAYERS_IN_ROOM_CODE + String.Format("{0:0000}", message.Length) + message);

            string response = this.communicator.ResponseFromServer();

            if (response.Contains("PlayersInRoom")) return response.Split(':')[1].Remove(response.Split(':')[1].Length - 1).Split('"')[1].Split(',');

            return null;
        }
        
        public bool JoinRoom(string roomName)
        {
            string message = "{\"roomName\": \"" + roomName + "\"}";

            this.communicator.RequestToServer((char)RoomRelatedCodes.JOIN_ROOM_CODE + String.Format("{0:0000}", message.Length) + message);

            string response = this.communicator.ResponseFromServer();

            return !response.Contains("message: ") || response.Contains("status: 1");
        }

        public bool CreateRoom(string roomName, string maxUser, string questionCount, string answerTimeout)
        {
            string message = "{\"roomName\": \"" + roomName + "\", \"maxUser\": \"" + maxUser + "\", \"questionCount\": \"" + questionCount + "\", \"answerTimeout\": \"" + answerTimeout + "\"}";

            this.communicator.RequestToServer((char)RoomRelatedCodes.CREATE_ROOM_CODE + String.Format("{0:0000}", message.Length) + message);

            string response = this.communicator.ResponseFromServer();

            return response.Contains("status: 1");
        }

        public string[] GetUserStats()
        {
            this.communicator.RequestToServer(((char)PersonalDataCodes.GET_PERSONAL_STATS_CODE).ToString());

            string response = this.communicator.ResponseFromServer();

            if (response.Contains("UserStatistics")) return response.Split(':')[1].Remove(response.Split(':')[1].Length - 1).Split('"')[1].Split(',');

            return null;
        }
        
        public string[] GetHighScores()
        {
            this.communicator.RequestToServer(((char)PersonalDataCodes.GET_HIGH_SCORES_CODE).ToString());

            string response = this.communicator.ResponseFromServer();

            if (response.Contains("HighScores")) return response.Split(':')[1].Remove(response.Split(':')[1].Length - 1).Split('"')[1].Split(',');

            return null;
        }

        public bool CloseRoom()
        {
            this.communicator.RequestToServer(((char)RoomAdminCodes.CLOSE_ROOM_CODE).ToString());

            string response = this.communicator.ResponseFromServer();

            return response.Contains("status: 1");
        }

        public bool StartGame()
        {
            this.communicator.RequestToServer(((char)RoomAdminCodes.START_GAME_CODE).ToString());

            string response = this.communicator.ResponseFromServer();

            return response.Contains("status: 1");
        }

        public string GetRoomDetails()
        {
            this.communicator.RequestToServer(((char)RoomAdminCodes.GET_ROOM_STATE_CODE).ToString());

            string response = this.communicator.ResponseFromServer();

            if (response.Contains("status: 1"))
            {
                return "Question Count: " + response.Split('"')[2].Split(':')[1].Split(',')[0]
                    + ", Answer Time Out: " + response.Split('"')[2].Split(':')[2].Split(',')[0];
            }
            else return "ERROR";
        }

        public string[] GetPlayersInRoomAfterJoin()
        {
            this.communicator.RequestToServer(((char)RoomAdminCodes.GET_ROOM_STATE_CODE).ToString());
            string response = this.communicator.ResponseFromServer();

            if (response.Contains("hasGameBegun")) return response.Split('"')[1].Split(',');
            else return null;
        }

        public bool LeaveRoom()
        {
            this.communicator.RequestToServer(((char)RoomMemberCode.LEAVE_ROOM_CODE).ToString());

            string response = this.communicator.ResponseFromServer();

            return response.Contains("message: ") || response.Contains("status: 1");
        }

        public int CheckRoomState()
        {
            this.communicator.RequestToServer(((char)RoomAdminCodes.GET_ROOM_STATE_CODE).ToString());

            string response = this.communicator.ResponseFromServer();

            if (response.Contains("status: 1")) return Int32.Parse(response.Split(':')[response.Split(':').Length - 1].Split('}')[0]);
            else return -1;
        }
    }
}
