﻿
namespace client_gui
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loginBtn = new System.Windows.Forms.Button();
            this.signUpBtn = new System.Windows.Forms.Button();
            this.userNameLogintextBox = new System.Windows.Forms.TextBox();
            this.passwordLogintextBox = new System.Windows.Forms.TextBox();
            this.quitBtn = new System.Windows.Forms.Button();
            this.mainTitle = new System.Windows.Forms.Label();
            this.WrongDetailsLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // loginBtn
            // 
            this.loginBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.loginBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.loginBtn.Font = new System.Drawing.Font("Segoe Print", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loginBtn.Location = new System.Drawing.Point(483, 138);
            this.loginBtn.Name = "loginBtn";
            this.loginBtn.Size = new System.Drawing.Size(261, 108);
            this.loginBtn.TabIndex = 0;
            this.loginBtn.Text = "Login";
            this.loginBtn.UseVisualStyleBackColor = false;
            this.loginBtn.Click += new System.EventHandler(this.loginBtn_Click);
            // 
            // signUpBtn
            // 
            this.signUpBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.signUpBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.signUpBtn.Font = new System.Drawing.Font("Segoe Print", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.signUpBtn.Location = new System.Drawing.Point(219, 280);
            this.signUpBtn.Name = "signUpBtn";
            this.signUpBtn.Size = new System.Drawing.Size(338, 73);
            this.signUpBtn.TabIndex = 2;
            this.signUpBtn.Text = "Sign Up";
            this.signUpBtn.UseVisualStyleBackColor = false;
            this.signUpBtn.Click += new System.EventHandler(this.signUpBtn_Click);
            // 
            // userNameLogintextBox
            // 
            this.userNameLogintextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.userNameLogintextBox.Font = new System.Drawing.Font("Modern No. 20", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userNameLogintextBox.Location = new System.Drawing.Point(179, 138);
            this.userNameLogintextBox.Name = "userNameLogintextBox";
            this.userNameLogintextBox.Size = new System.Drawing.Size(203, 36);
            this.userNameLogintextBox.TabIndex = 3;
            this.userNameLogintextBox.Tag = "";
            this.userNameLogintextBox.Text = "user1";
            // 
            // passwordLogintextBox
            // 
            this.passwordLogintextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.passwordLogintextBox.Font = new System.Drawing.Font("Modern No. 20", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordLogintextBox.Location = new System.Drawing.Point(179, 210);
            this.passwordLogintextBox.Name = "passwordLogintextBox";
            this.passwordLogintextBox.Size = new System.Drawing.Size(203, 36);
            this.passwordLogintextBox.TabIndex = 4;
            this.passwordLogintextBox.Text = "1234";
            // 
            // quitBtn
            // 
            this.quitBtn.BackColor = System.Drawing.Color.PaleTurquoise;
            this.quitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.quitBtn.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quitBtn.Location = new System.Drawing.Point(272, 381);
            this.quitBtn.Name = "quitBtn";
            this.quitBtn.Size = new System.Drawing.Size(228, 46);
            this.quitBtn.TabIndex = 6;
            this.quitBtn.Text = "quit";
            this.quitBtn.UseVisualStyleBackColor = false;
            this.quitBtn.Click += new System.EventHandler(this.quitBtn_Click);
            // 
            // mainTitle
            // 
            this.mainTitle.AutoSize = true;
            this.mainTitle.Font = new System.Drawing.Font("Goudy Stout", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainTitle.Location = new System.Drawing.Point(23, 3);
            this.mainTitle.Name = "mainTitle";
            this.mainTitle.Size = new System.Drawing.Size(749, 132);
            this.mainTitle.TabIndex = 8;
            this.mainTitle.Text = "Trivia";
            // 
            // WrongDetailsLabel
            // 
            this.WrongDetailsLabel.AutoSize = true;
            this.WrongDetailsLabel.Font = new System.Drawing.Font("Miriam Fixed", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WrongDetailsLabel.ForeColor = System.Drawing.Color.Red;
            this.WrongDetailsLabel.Location = new System.Drawing.Point(495, 249);
            this.WrongDetailsLabel.Name = "WrongDetailsLabel";
            this.WrongDetailsLabel.Size = new System.Drawing.Size(236, 11);
            this.WrongDetailsLabel.TabIndex = 9;
            this.WrongDetailsLabel.Text = "Wrong Details / Already logged in";
            this.WrongDetailsLabel.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Modern No. 20", 20.25F);
            this.label2.Location = new System.Drawing.Point(49, 213);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 29);
            this.label2.TabIndex = 17;
            this.label2.Text = "password:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 20.25F);
            this.label1.Location = new System.Drawing.Point(49, 141);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 29);
            this.label1.TabIndex = 16;
            this.label1.Text = "username:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.WrongDetailsLabel);
            this.Controls.Add(this.mainTitle);
            this.Controls.Add(this.quitBtn);
            this.Controls.Add(this.passwordLogintextBox);
            this.Controls.Add(this.userNameLogintextBox);
            this.Controls.Add(this.signUpBtn);
            this.Controls.Add(this.loginBtn);
            this.Name = "MainForm";
            this.Text = "Trivia";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button loginBtn;
        private System.Windows.Forms.Button signUpBtn;
        private System.Windows.Forms.TextBox userNameLogintextBox;
        private System.Windows.Forms.TextBox passwordLogintextBox;
        private System.Windows.Forms.Button quitBtn;
        private System.Windows.Forms.Label mainTitle;
        private System.Windows.Forms.Label WrongDetailsLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}

