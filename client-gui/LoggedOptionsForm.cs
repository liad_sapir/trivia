﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net.Sockets;
using System.Net;

namespace client_gui
{
    public partial class LoggedOptionsForm : Form
    {
        private Client _client;

        public LoggedOptionsForm(NetworkStream clientStream, string name)
        {
            InitializeComponent();
            this._client = new Client(clientStream);
            this.name.Text = name;
        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            //hiding form
            this.Hide();

            //signing out
            this._client.Logout();

            //moving to home form
            Form wnd = new MainForm();
            wnd.ShowDialog();

            //closing form
            this.Close();
        }

        private void joinRoomBtn_Click(object sender, EventArgs e)
        {
            //hiding form
            this.Hide();

            //moving to next form
            Form wnd = new JoinRoomForm(this._client.GetStream(), this.name.Text);
            wnd.ShowDialog();

            //closing form
            this.Close();
        }

        private void createRoomBtn_Click(object sender, EventArgs e)
        {
            //hiding form
            this.Hide();

            //moving to next form
            Form wnd = new CreateRoomForm(this._client.GetStream(), this.name.Text);
            wnd.ShowDialog();

            //closing form
            this.Close();
        }

        private void bestScoresBtn_Click(object sender, EventArgs e)
        {
            //hiding form
            this.Hide();

            //moving to next form
            Form wnd = new BestScoresForm(this._client.GetStream(), this.name.Text);
            wnd.ShowDialog();

            //closing form
            this.Close();
        }

        private void myStatusBtn_Click(object sender, EventArgs e)
        {
            //hiding form
            this.Hide();

            //moving to next form
            Form wnd = new MyStatusForm(this._client.GetStream(), this.name.Text);
            wnd.ShowDialog();

            //closing form
            this.Close();
        }

        private void LoggedOptionsForm_Load(object sender, EventArgs e)
        {

        }
    }
}
