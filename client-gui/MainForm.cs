﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net.Sockets;
using System.Net;

namespace client_gui
{
    public partial class MainForm : Form
    {
        private Client _client;

        public MainForm()
        {
            InitializeComponent();
            this._client = new Client(); 
        }

        public MainForm(NetworkStream clientStream)
        {
            InitializeComponent();
            this._client = new Client(clientStream);
        }

        private void loginBtn_Click(object sender, EventArgs e)
        {
            if(this._client.Login(userNameLogintextBox.Text, passwordLogintextBox.Text))
            {
                //hiding form
                this.Hide();

                //moving to next form
                Form wnd = new LoggedOptionsForm(this._client.GetStream(), userNameLogintextBox.Text);
                wnd.ShowDialog();

                //closing form
                this.Close();
            }
            else
            {
                //message for wrong details
                WrongDetailsLabel.Visible = true;
            }
        }

        private void signUpBtn_Click(object sender, EventArgs e)
        {
            //hiding form
            this.Hide();

            //moving to next form
            Form wnd = new SignUpForm(this._client.GetStream());
            wnd.ShowDialog();

            //closing form
            this.Close();
        }

        private void quitBtn_Click(object sender, EventArgs e)
        {
            //closing form
            this.Close();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

    }
}
