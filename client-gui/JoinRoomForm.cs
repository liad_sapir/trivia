﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net.Sockets;
using System.Net;

using System.Threading;

namespace client_gui
{
    public partial class JoinRoomForm : Form
    {
        private Client _client;
        private Thread roomsThread;
        public JoinRoomForm(NetworkStream clientStream, string name)
        {
            InitializeComponent();
            this._client = new Client(clientStream);
            this.name.Text = name;
        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            //hiding form
            this.Hide();

            this.roomsThread.Abort();

            //moving to next form
            Form wnd = new LoggedOptionsForm(this._client.GetStream(), this.name.Text);
            wnd.ShowDialog();

            //closing form
            this.Close();
        }

        private void listBoxRooms_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(this.listBoxRooms.SelectedIndex != -1)
            {
                //hiding form
                this.Hide();
                this.roomsThread.Abort();

                //moving to next form
                Form wnd = new ConnectedToRoomForm(false, this.listBoxRooms.Items[this.listBoxRooms.SelectedIndex].ToString(), this._client.GetStream(), this.name.Text);
                wnd.ShowDialog();

                //closing form
                this.Close();
            }
        }

        private void updateRoom(string room)
        {
            this.listBoxRooms.BeginUpdate();
            this.listBoxRooms.Items.Add(room);
            this.listBoxRooms.EndUpdate();
        }

        private void clearRooms()
        {
            this.listBoxRooms.BeginUpdate();
            this.listBoxRooms.Items.Clear();
            this.listBoxRooms.EndUpdate();
        }

        private delegate void DisplayRoomDelegate(string room);

        private delegate void DisplayCleanDelegate();

        private void GetRooms()
        {
            while (true)
            {
                this.listBoxRooms.Invoke(new DisplayCleanDelegate(clearRooms));
                string[] rooms= this._client.GetRooms();
                if (rooms != null && rooms.Length != 0) for (int i = 0; i < rooms.Length; i++) this.listBoxRooms.Invoke(new DisplayRoomDelegate(updateRoom), rooms[i]);
                Thread.Sleep(3000);
            }
        }

        private void JoinRoomForm_Load(object sender, EventArgs e)
        {
            //thread for getting rooms every few seconds
            this.roomsThread = new Thread(new ThreadStart(this.GetRooms));
            this.roomsThread.IsBackground = true;
            this.roomsThread.Start();
        }
    }
}
