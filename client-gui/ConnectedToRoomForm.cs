﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net.Sockets;
using System.Net;

using System.Threading;

namespace client_gui
{
    public partial class ConnectedToRoomForm : Form
    {
        private Client _client;
        private string roomName;
        private Thread UpdateStateThread;
        private bool joined;
        private bool admin;
        public ConnectedToRoomForm(bool admin, string roomName, NetworkStream clientStream, string name)
        {
            InitializeComponent();
            this._client = new Client(clientStream);
            this.name.Text = name;
            this.RoomNameLabel.Text = roomName;
            this.roomName = roomName;
            if (admin)
            {
                this.admin = true;
                this.labelRoomDetails.Text = this._client.GetRoomDetails();
                this.btn1.Text = "START";
                this.btn2.Text = "CLOSE";
                this.btn1.Click += (object sender, EventArgs e) =>
                {
                    if (this._client.StartGame())
                    {
                        this.UpdateStateThread.Abort();
                        MessageBox.Show("Game Started");
                    }
                };
                this.btn2.Click += (object sender, EventArgs e) =>
                {
                    if (this._client.CloseRoom())
                    {
                        this.UpdateStateThread.Abort();
                        MessageBox.Show("Room Closed");
                        GoBack();
                    }
                };
            }
            else
            {
                this.btn1.Text = "JOIN";
                this.btn2.Text = "BACK";
                //defining the button to a join room button
                this.btn1.Click += (object sender, EventArgs e) =>
                {
                    try
                    {
                        this.UpdateStateThread.Suspend();
                        if (this.joined)
                        {
                            if (this._client.LeaveRoom())
                            {
                                this.UpdateStateThread.Resume();
                                this.UpdateStateThread.Abort();
                                GoBack();
                            }
                        }
                        else if (this._client.JoinRoom(roomName))
                        {
                            this.joined = true;
                            this.labelRoomDetails.Text = this._client.GetRoomDetails();
                            MessageBox.Show("Joined room");

                            //changing the button to leave button
                            this.btn1.Text = "LEAVE";
                            btn2.Visible = false;
                        }
                        this.UpdateStateThread.Resume();
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    
                };
                //defining the button to go to join room form
                this.btn2.Click += (object sender, EventArgs e) =>
                {
                    this.UpdateStateThread.Abort();
                    GoBack();
                };
            }
        }

        private void GoBack()
        {
            //hiding form
            this.Hide();

            //moving to next form
            Form wnd = new LoggedOptionsForm(this._client.GetStream(), this.name.Text);
            
            wnd.ShowDialog();

            //closing form
            this.Close();
        }

        private void updatePlayer(string player)
        {
            this.listBoxPlayers.BeginUpdate();
            this.listBoxPlayers.Items.Add(player);
            this.listBoxPlayers.EndUpdate();
        }

        private void clearPlayers()
        {
            this.listBoxPlayers.BeginUpdate();
            this.listBoxPlayers.Items.Clear();
            this.listBoxPlayers.EndUpdate();
        }

        private delegate void DisplayPlayerDelegate(string player);

        private delegate void DisplayCleanDelegate();

        private delegate void GoBackDelegate();

        private bool GetPlayers()
        {
            this.listBoxPlayers.Invoke(new DisplayCleanDelegate(clearPlayers));
            string[] players;
            if (!this.joined && !this.admin) players = this._client.GetPlayersInRoom(this.roomName);
            else players = this._client.GetPlayersInRoomAfterJoin();
            if (players != null && players.Length != 0)
            {
                for (int i = 0; i < players.Length; i++) this.listBoxPlayers.Invoke(new DisplayPlayerDelegate(updatePlayer), players[i]);
                return true;
            }
            MessageBox.Show("Room Closed/Started");
            return false;
        }

        private bool CheckRoomState()
        {
            if (!this.admin && this.joined)
            {
                switch(this._client.CheckRoomState())
                {
                    case -1:
                        MessageBox.Show("Room has been closed");
                        return false;
                    case 1:
                        MessageBox.Show("Game Started");
                        if (!this.joined) return false;
                        break;
                    default:
                        break;
                }
            }
            return true;
        }

        private void UpdateThread()
        {
            //getting players names and checking if the room has been closed or if the game has been started
            while (this.GetPlayers() && this.CheckRoomState())
            {
                Thread.Sleep(5000);
            }
            try
            {
                this.Invoke((MethodInvoker)delegate
                {
                    this.GoBack();
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void ConnectedToRoomForm_Load(object sender, EventArgs e)
        {
            //thread for update things every few seconds
            this.UpdateStateThread = new Thread(new ThreadStart(this.UpdateThread));
            this.UpdateStateThread.IsBackground = true;
            this.UpdateStateThread.Priority = ThreadPriority.Highest;
            this.UpdateStateThread.Start();
        }
    }
}
