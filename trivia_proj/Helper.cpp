#include "Helper.h"
#include <iomanip>
#include <sstream>
#include <stdlib.h>
#include "IRequestHandler.h"
#include <jsoncons/json.hpp>
#include <jsoncons_ext/jsonpath/jsonpath.hpp>


std::string Helper::getPaddedNumber(int num, int digits)
{
    std::ostringstream ostr;
    ostr << std::setw(digits) << std::setfill('0') << num;
    return ostr.str();

}

void Helper::updateResultVector(std::vector<unsigned char>& v, std::string message, unsigned char code) {
    int i = 0;
    std::string data_length = Helper::getPaddedNumber(message.length(), 4);
    v.push_back(code);
    for (i = 0; i < data_length.length(); i++) v.push_back(data_length[i]);
    for (i = 0; i < message.length(); i++) v.push_back(message[i]);
}

int Helper::getReqSize(std::vector<unsigned char> v) {
    return (v[thousands] - '0') * 1000 + (v[hundreds] - '0') * 100 + (v[tens] - '0') * 10 + v[units] - '0';
}

std::string Helper::takeUserName(std::string message) {
    std::string username = "";
    int i = 0;
    for (i = name_start_index; message[i] != '"'; i++) username += message[i];
    return username;
}

std::string Helper::takePassword(std::string message) {
    std::string password = "";
    int i = 0;
    for (i = name_start_index; message[i] != ' '; i++);
    for (i = i + password_start_index_helper; message[i] != '"'; i++) password += message[i];
    return password;
}

std::string Helper::takeEmail(std::string message) {
    std::string email = "";
    int i = 0;
    for (i = name_start_index; message[i] != ' '; i++) {
    
    }
    for (i = i + password_start_index_helper; message[i] != ' '; i++) {

    }
    for (i = i + mail_start_index_helper; message[i] != '"'; i++) {
        email += message[i];
    }
    return email;
}

std::vector<unsigned char>  Helper::to_vector(char* message) {
    std::vector<unsigned char> res_vector;
    int i = 0;
    for (i = 0; message[i] != '\0' ; i++) {
        res_vector.push_back(message[i]);
    }
    return res_vector;
}

char* Helper::to_char(std::vector<unsigned char> v) {
    char message[1024] = { 0 };
    int i = 0;
    for (i = 0; i < v.size(); i++) message[i] = v[i];
    message[i] = '\0';
    return message;
}

int checkReqType(RequestInfo ri) {
    return 0;
}

jsoncons::json Helper::extractData(std::vector<unsigned char> buffer) {
    jsoncons::json data_json;
    std::string data = Helper::takeMessage(buffer);
    data_json = jsoncons::json::parse(data);
    return data_json;
}

std::string Helper::takeMessage(std::vector<unsigned char> buffer) {
    std::string message = "";
    for (int i = 5; i < buffer.size(); i++) message += buffer[i];
    return message;
}

void Helper::eraseMessageData(char* m) {
    for (int i = 0;i < DEFAULT_SIZE; i++) m[i] = 0;
}

bool Helper::is_sock_alive(SOCKET sock) {
    int r = recv(sock, NULL, 0, 0);
    if (r == SOCKET_ERROR && WSAGetLastError() == WSAECONNRESET) return false;
    return true;
}

std::vector<unsigned char> Helper::onlyStatusRes(unsigned int status, unsigned char code) {
    std::string message_string = "{status: " + std::to_string(status) + '}';
    std::vector<unsigned char> result_vector;
    Helper::updateResultVector(std::ref(result_vector), message_string, code);
    return result_vector;
}