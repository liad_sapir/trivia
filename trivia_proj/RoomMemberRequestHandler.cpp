#pragma once
#include "RoomMemberRequestHandler.h"
#include "jsonResponsePacketSerializer.h"

RoomMemberRequestHandler::RoomMemberRequestHandler(Room room, LoggedUser lu, RoomManager& roomManager, RequestHandlerFactory& rhf) : m_room(room), m_user(lu), m_roomManager(roomManager), m_requestHandlerFactory(rhf) {

}

RoomMemberRequestHandler::~RoomMemberRequestHandler() {

}

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo ri) {
	unsigned char id = ri.buffer[0];
	return (id == LEAVE_ROOM_CODE || id == GET_ROOM_STATE_CODE);
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo ri) {
	RequestResult rr;
	ErrorResponse er = { "not right handler changing now!" };
	unsigned char id = ri.buffer[0];
	if (this->isRequestRelevant(ri)) {
		switch (id) {
		case LEAVE_ROOM_CODE:
			rr = this->leaveRoom(ri);
			break;
		case GET_ROOM_STATE_CODE:
			rr = this->getRoomState(ri,false);
			break;
		}
	}
	else {
		rr.buffer = JsonResponsePacketSerializer::serializeResponse(er);
		rr.newHandler = this->m_requestHandlerFactory.createMenuRequestHandler(this->m_user);
	}
	return rr;
}

RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo ri) {
	RequestResult rr;
	LeaveRoomResponse lrr = { 1 };
	this->m_room.removeUser(this->m_user);
	this->m_roomManager.updateRoom(this->m_room);
	rr.buffer = JsonResponsePacketSerializer::serializeResponse(lrr);
	rr.newHandler = this->m_requestHandlerFactory.createMenuRequestHandler(this->m_user);
	return rr;
}

RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo ri, bool admin) {
	RequestResult rr;
	GetRoomStateResponse grsr;
	this->m_room = this->m_roomManager.getRoomByName(this->m_room.getData().name);
	RoomData rd = this->m_room.getData();
	grsr.players = this->m_room.getAllUsers();
	grsr.answerTimeout = rd.timePerQuestion;
	grsr.questionCount = rd.numOfQuestionsInGame;
	grsr.hasGameBegun = rd.isActive;
	grsr.status = 1;
	rr.buffer = JsonResponsePacketSerializer::serializeResponse(grsr);
	if (admin) rr.newHandler = this->m_requestHandlerFactory.createRoomAdminRequestHandler(this->m_room, this->m_user);
	else rr.newHandler = this->m_requestHandlerFactory.createRoomMemberRequestHandler(this->m_room, this->m_user);
	return rr;
}

void RoomMemberRequestHandler::logoutPlayer(RequestInfo ri) {
	this->m_room.removeUser(this->m_user);
	this->m_roomManager.updateRoom(this->m_room);
	this->m_requestHandlerFactory.getLoginManager().logout(this->m_user.getUsername());
}


std::string RoomMemberRequestHandler::getTypeOfHandler() {
	return "member handler";
}