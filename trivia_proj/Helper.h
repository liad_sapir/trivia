#pragma once
#include <WinSock2.h>
#include <iostream>
#include <vector>
#include <jsoncons/json.hpp>
#include <jsoncons_ext/jsonpath/jsonpath.hpp>

#define DEFAULT_SIZE 200

class Helper {
public:
	static std::string getPaddedNumber(int num, int digits);
	static void updateResultVector(std::vector<unsigned char> & v, std::string message, unsigned char code);
	static int getReqSize(std::vector<unsigned char> v);
	static std::string takeUserName(std::string message);
	static std::string takePassword(std::string message);
	static std::string takeEmail(std::string message);
	static std::vector<unsigned char>  to_vector(char* message);
	static char* to_char(std::vector<unsigned char> v);
	static jsoncons::json extractData(std::vector<unsigned char> buffer);
	static std::string takeMessage(std::vector<unsigned char> buffer);
	static void eraseMessageData(char* m);
	static bool is_sock_alive(SOCKET sock);
	static std::vector<unsigned char> onlyStatusRes(unsigned int status, unsigned char code);
private:
	enum helpers {
		units = 4,
		tens = 3,
		hundreds = 2,
		thousands = 1,
		name_start_index = 12,
		password_start_index_helper = 12,
		mail_start_index_helper = 9
	};
};