#ifndef ROOMMEMBERREQUESTHANDLER_H
#define ROOMMEMBERREQUESTHANDLER_H

#include "RequestHandlerFactory.h"

class RoomMemberRequestHandler : public IRequestHandler {
public:
	RoomMemberRequestHandler(Room room, LoggedUser lu, RoomManager& roomManager, RequestHandlerFactory& rhf);
	~RoomMemberRequestHandler();
	virtual bool isRequestRelevant(RequestInfo ri);
	virtual RequestResult handleRequest(RequestInfo ri);
	virtual void logoutPlayer(RequestInfo ri) override;
	virtual std::string getTypeOfHandler();
	RequestResult getRoomState(RequestInfo ri, bool admin);
	
private:
	Room m_room;
	LoggedUser m_user;
	RoomManager& m_roomManager;
	RequestHandlerFactory& m_requestHandlerFactory;
	RequestResult leaveRoom(RequestInfo ri);
};

#endif // !ROOMMEMBERREQUESTHANDLER_H
