#pragma once 

#include "RequestHandlerFactory.h"

class MenuRequestHandler : public IRequestHandler {
public:
	MenuRequestHandler(LoggedUser user, RoomManager& rm, RequestHandlerFactory& rhf);
	~MenuRequestHandler();
	virtual std::string getTypeOfHandler();
	virtual void logoutPlayer(RequestInfo ri) override;
	virtual bool isRequestRelevant(RequestInfo ri);
	virtual RequestResult handleRequest(RequestInfo ri);
private:
	LoggedUser m_user;
	RoomManager& m_roomManager;
	RequestHandlerFactory& m_handlerFactory;
	RequestResult signout(RequestInfo ri);
	RequestResult getRooms(RequestInfo ri);
	RequestResult getPlayersInRoom(RequestInfo ri);
	RequestResult joinRoom(RequestInfo ri);
	RequestResult createRoom(RequestInfo ri);
	RequestResult getStatistics(RequestInfo ri);
	RequestResult getHighScores(RequestInfo ri);
	enum defines {
		ID_INDEX = 0,
	};
};