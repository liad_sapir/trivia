#include "LoggedUser.h"

LoggedUser::LoggedUser(std::string username, SOCKET sock) {
	this->_username = username;
	this->_client_sock = sock;
}

std::string LoggedUser::getUsername() const {
	return this->_username;
}

SOCKET LoggedUser::getClientSocket() const {
	return this->_client_sock;
}