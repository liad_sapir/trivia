#include "RoomManager.h"

RoomManager::RoomManager() {

}

RoomManager::~RoomManager() {

}

bool RoomManager::createRoom(LoggedUser lu, RoomData rd) {
	Room new_room = Room(rd);
	new_room.addUser(lu);
	std::map<unsigned int, Room>::iterator room_iter;
	for (room_iter = this->m_rooms.begin(); room_iter != this->m_rooms.end(); ++room_iter) {
		if (room_iter->second.getData().name == rd.name) return false;
	}
	this->m_rooms.insert(std::pair<unsigned int, Room>(rd.id, new_room));
	return true;
}

void RoomManager::deleteRoom(std::string name) {
	std::map<unsigned int, Room>::iterator map_iter;
	for (map_iter = this->m_rooms.begin(); map_iter != this->m_rooms.end(); ++map_iter) {
		if (map_iter->second.getData().name == name) {
			this->m_rooms.erase(map_iter);
			break;
		}
	}
}

unsigned int RoomManager::getRoomState(unsigned int id) {
	std::map<unsigned int, Room>::iterator map_iter;
	for (map_iter = this->m_rooms.begin(); map_iter != this->m_rooms.end(); ++map_iter) {
		if (map_iter->first == id) return map_iter->second.getData().isActive;
	}
	return -1;
}

std::vector<RoomData> RoomManager::getRooms() {
	std::vector<RoomData> vrd;
	std::map<unsigned int, Room>::iterator map_iter;
	for (map_iter = this->m_rooms.begin(); map_iter != this->m_rooms.end(); ++map_iter) {
		vrd.push_back(map_iter->second.getData());
	}
	return vrd;
}

Room RoomManager::getRoomByName(std::string name) {
	std::map<unsigned int, Room>::iterator room_iter;
	for (room_iter = this->m_rooms.begin(); room_iter != this->m_rooms.end(); ++room_iter) {
		if (room_iter->second.getData().name == name) return room_iter->second;
	}
	return Room(RoomData{0,"defualt",0,0,0,-1}); //just to let the code compile cause the compiler searches a return value also after the for loop 
}

void RoomManager::updateRoom(Room room) {
	std::map<unsigned int, Room>::iterator room_iter;
	for (room_iter = this->m_rooms.begin(); room_iter != this->m_rooms.end(); ++room_iter) {
		if (room_iter->second.getData().name == room.getData().name) {
			room_iter->second = room;
			return;
		}
	}
}