#ifndef LOGINREQUESTHANDLER_H
#define LOGINREQUESTHANDLER_H

#include <iostream>
#include <ctime>
#include <vector>
#include "IRequestHandler.h"

class LoginManager;
class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler {
public:
	LoginRequestHandler(LoginManager& lm, RequestHandlerFactory& rhf);
	virtual ~LoginRequestHandler();
	virtual std::string getTypeOfHandler();
	bool isRequestRelevant(RequestInfo ri) override;
	RequestResult handleRequest(RequestInfo ri);

private:
	LoginManager& m_loginManager;
	RequestHandlerFactory& m_handlerFactory;

	RequestResult login(RequestInfo ri);
	RequestResult signUp(RequestInfo ri);
	
	int checkReqType(RequestInfo ri);
};

#endif // !LOGINREQUESTHANDLER_H
