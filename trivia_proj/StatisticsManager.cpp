#include "StatisticsManager.h"

#include "SqliteDatabase.h"

#include <algorithm>

#define TOP 5

StatisticsManager::StatisticsManager() {

}

StatisticsManager::StatisticsManager(IDatabase* database) : m_database(database) {
	
}

StatisticsManager::~StatisticsManager() {

}

std::vector<std::string> StatisticsManager::getHighScore() {
	std::vector<std::string> res = this->m_database->getLoggedUsers(), res_top;
	for (int i = 0; i < res.size(); i++) {
		res[i] = res[i] + " : " + std::to_string(((this->m_database->getNumOfCorrectAnswers(res[i]) * this->m_database->getPlayerAverageAnswerTime(res[i])) / this->m_database->getNumOfTotalAnswers(res[i])) * 100);
	}
	sort(res.begin(), res.end());
	for (int i = res.size()- 1; i > res.size() - TOP; i--) res_top.push_back(res[i]);
	return res_top;
}

std::vector<std::string> StatisticsManager::getUserStatistics(std::string username) {
	std::vector<std::string> stats;
	stats.push_back(std::to_string(this->m_database->getNumOfPlayerGames(username)));
	stats.push_back(std::to_string(this->m_database->getNumOfTotalAnswers(username)));
	stats.push_back(std::to_string(this->m_database->getPlayerAverageAnswerTime(username)));	
	stats.push_back(std::to_string(this->m_database->getNumOfCorrectAnswers(username)));
	return stats;
}