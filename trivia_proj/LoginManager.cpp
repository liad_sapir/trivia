#include "LoginManager.h"
#include "SqliteDatabase.h"

LoginManager::LoginManager() {
	this->m_database = new SqliteDatabase();
}

LoginManager::LoginManager(IDatabase* database) {
	this->m_database = database;
}

LoginManager::~LoginManager() {

}

bool LoginManager::signUp(std::string username, std::string password, std::string email) {
	if (!this->m_database->doesUserExist(username)) {
		this->m_database->addNewUser(username, password, email);
		return true;
	}
	else return false;
}

LoggedUser LoginManager::login(std::string username, std::string password, SOCKET client_sock) {
	LoggedUser lu(username, client_sock);
	if (!this->userAlreadyLogged(username) && m_database->doesPasswordMatch(username, password)) {
		this->m_loggedUsers.push_back(lu);
	}
	else lu = LoggedUser(" ", NULL);
	return lu;
}

bool LoginManager::logout(std::string username) {
	std::vector<LoggedUser>::iterator lui;
	for (lui = this->m_loggedUsers.begin(); lui != this->m_loggedUsers.end(); ++lui) {
		if (lui->getUsername() == username) {
			this->m_loggedUsers.erase(lui);
			return true;
		}
	}
	return false;
}

bool LoginManager::userAlreadyLogged(std::string username) {
	int i = 0;
	for (i = 0; i < this->m_loggedUsers.size(); i++) {
		if (this->m_loggedUsers[i].getUsername() == username) return true;
	}
	return false;
}