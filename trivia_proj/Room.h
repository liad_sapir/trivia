#ifndef ROOM_H
#define ROOM_H

#include <iostream>
#include <vector>

#include "LoggedUser.h"
#include "RoomData.h"

class Room {
public:
	Room();
	Room(RoomData rd);
	~Room();
	bool addUser(LoggedUser lu);
	void removeUser(LoggedUser lu);
	std::vector<std::string> getAllUsers();
	std::vector<SOCKET> getAllUsersSockets();
	RoomData getData();
	void setActive();

private:
	RoomData m_metadata;
	std::vector<LoggedUser> m_users;
};

#endif // !ROOM_H