#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"

void main(){
	try {
		WSAInitializer wsaInit;
		Server s;

		s.run(); //opening server on 127.0.0.1:7777 (port for luck)
	}
	catch (std::exception& e) {
		std::cout << e.what() << std::endl;
	}
}