#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "Response.h"
#include "ids.h"

class JsonResponsePacketSerializer {
public:
    static std::vector<unsigned char> serializeResponse(ErrorResponse res);
    static std::vector<unsigned char> serializeResponse(LoginResponse res);
    static std::vector<unsigned char> serializeResponse(SignUpResponse res);
    static std::vector<unsigned char> serializeResponse(LogoutResponse res);
    static std::vector<unsigned char> serializeResponse(GetRoomsResponse res);
    static std::vector<unsigned char> serializeResponse(GetPlayersInRoomResponse res);
    static std::vector<unsigned char> serializeResponse(JoinRoomResponse res);
    static std::vector<unsigned char> serializeResponse(CreateRoomResponse res);
    static std::vector<unsigned char> serializeResponse(GetPersonalStatsResponse res);
    static std::vector<unsigned char> serializeResponse(GetHighScoreResponse res);
    static std::vector<unsigned char> serializeResponse(CloseRoomResponse res);
    static std::vector<unsigned char> serializeResponse(StartGameResponse res);
    static std::vector<unsigned char> serializeResponse(GetRoomStateResponse res);
    static std::vector<unsigned char> serializeResponse(LeaveRoomResponse res);
private:

};