#pragma once

#include <winSock2.h>
#include <iostream>
#include <map>
#include <mutex>

#include "LoginManager.h"
#include "LoginRequestHandler.h"
#include "RoomManager.h"
#include "MenuRequestHandler.h"
#include "ids.h"
#include "StatisticsManager.h"
#include "RoomMemberRequestHandler.h"
#include "RoomAdminRequestHandler.h"

class LoginManager;
class IDatabase;
class MenuRequestHandler;
class RoomMemberRequestHandler;
class RoomAdminRequestHandler;

class RequestHandlerFactory {
public:
	RequestHandlerFactory();
	~RequestHandlerFactory();
	void setClientsMap(std::map<SOCKET, IRequestHandler*>* clientsMap);
	void setClientHandler(SOCKET clientSock, IRequestHandler* newHandler);
	void setDataBase(IDatabase* db);
	LoginRequestHandler* createLoginRequestHandler();
	LoginManager& getLoginManager();
	MenuRequestHandler* createMenuRequestHandler(LoggedUser lu);
	StatisticsManager& getStatisticsManager();
	RoomManager& getRoomManager();
	RoomMemberRequestHandler* createRoomMemberRequestHandler(Room m_room, LoggedUser lu);
	RoomAdminRequestHandler* createRoomAdminRequestHandler(Room m_room, LoggedUser lu);

private:
	IDatabase* m_database;
	LoginManager m_loginManager;
	RoomManager m_roomManager;
	StatisticsManager m_StatisticsManager;
	std::map<SOCKET, IRequestHandler*>* m_clientsHandlers;
};