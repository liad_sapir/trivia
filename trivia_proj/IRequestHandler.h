#ifndef IREQUESTHANDLER_H
#define IREQUESTHANDLER_H

#include <WinSock2.h>
#include <iostream>
#include <vector>

#include "Request.h"

class IRequestHandler;

typedef struct RequestResult {
	std::vector<unsigned char> buffer;
	IRequestHandler* newHandler;
}RequestResult;

typedef struct RequestInfo {
	unsigned int id;
	char* receivalTime;
	std::vector<unsigned char> buffer;
	SOCKET client_sock;
}RequestInfo;

class IRequestHandler {
public:
	virtual bool isRequestRelevant(RequestInfo req) = 0;
	virtual RequestResult handleRequest(RequestInfo req) = 0;
	virtual std::string getTypeOfHandler() = 0;
	virtual void logoutPlayer(RequestInfo ri) { };
};

#endif // !IREQUESTHANDLER_H
