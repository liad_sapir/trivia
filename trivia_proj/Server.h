#pragma once
#include <iostream>
#include <thread>
#include "Communicator.h"
#include "RequestHandlerFactory.h"
#include "IDatabase.h"

class Server{
public:
	Server();
	~Server();
	void run();
private:
	Communicator m_communicator;
};