#ifndef LOGGEDUSER_H
#define LOGGEDUSER_H

#include <WinSock2.h>
#include <iostream>

class LoggedUser {
public:
	LoggedUser(std::string username, SOCKET sock);
	std::string getUsername() const;
	SOCKET getClientSocket() const;

private:
	std::string _username;
	SOCKET _client_sock;
};

#endif // !LOGGEDUSER_H
