#ifndef LOGINMANAGER_H
#define LOGINMANAGER_H

#include "LoggedUser.h"
#include <vector>
#include "IDatabase.h"

class LoginManager {
public:
	LoginManager();
	LoginManager(IDatabase* database);
	~LoginManager();
	bool signUp(std::string username, std::string password, std::string email);
	LoggedUser login(std::string username, std::string password, SOCKET client_sock);
	bool logout(std::string username);
	bool userAlreadyLogged(std::string username);
	LoggedUser getLastUser() {
		return *(this->m_loggedUsers.end());
	}

private:
	std::vector<LoggedUser> m_loggedUsers;
	IDatabase* m_database;
};

#endif // !LOGINMANAGER_H
