#pragma once

#include "RequestHandlerFactory.h"

class RoomMemberRequestHandler;
class RoomAdminRequestHandler : public IRequestHandler {
public:
	RoomAdminRequestHandler(Room m_room, LoggedUser m_user, RoomManager& roomManager, RequestHandlerFactory& rhf);
	~RoomAdminRequestHandler();
	virtual bool isRequestRelevant(RequestInfo ri);
	virtual RequestResult handleRequest(RequestInfo ri);
	virtual void logoutPlayer(RequestInfo ri) override;
	virtual std::string getTypeOfHandler();
private:
	Room m_room;
	LoggedUser m_user;
	RoomManager& m_roomManager;
	RequestHandlerFactory& m_handlerFactory;
	RoomMemberRequestHandler* m_handlerHelper;
	RequestResult closeRoom(RequestInfo ri);
	RequestResult startGame(RequestInfo ri);
	void changeUsersHandlers(SOCKET mainSocket, std::vector<SOCKET> usersSockets, std::vector<std::string> usersNames, int type);
	enum code {
		CLOSE_GAME,
		START_GAME
	};
};