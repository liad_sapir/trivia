import socket

#defining address of server
HOST = '127.0.0.1'
PORT = 7777    

messages = [b'10041{"username": "user2", "password": "1234"}', b'4', b'60021{"roomName": "room1"}', b'C', b'C', b'C', b'C', b'C', b'C', b'C']

#making socket instance
client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_sock.connect((HOST, PORT)) #connecting to server

for message in messages:    
    client_sock.sendall(message)
    data = client_sock.recv(1024)
    print('recived: ' + data.decode()) #printing message from Server
