#pragma once
enum CodesLogin{
    ERROR_CODE = '0',
    LOGIN_CODE = '1',
    SIGNUP_CODE = '2',
    LOGOUT_CODE = '3'
};

enum RoomRelatedCodes {
    GET_ROOMS_CODE = '4',
    GET_PLAYERS_IN_ROOM_CODE = '5',
    JOIN_ROOM_CODE = '6',
    CREATE_ROOM_CODE = '7',
};

enum PersonalDataCodes {
    GET_PERSONAL_STATS_CODE = '8',
    GET_HIGH_SCORES_CODE = '9',
};

enum RoomAdminCodes {
    CLOSE_ROOM_CODE = 'A',
    START_GAME_CODE = 'B',
    GET_ROOM_STATE_CODE = 'C',
};

enum RoomMemberCode{
    LEAVE_ROOM_CODE = 'D'
};