#include "Room.h"

Room::Room() {

}

Room::Room(RoomData rd) {
	this->m_metadata = rd;
}

Room::~Room() {

}

bool Room::addUser(LoggedUser lu) {
	if (this->m_users.size() < this->m_metadata.maxPlayers) {
		this->m_users.push_back(lu);
		return true;
	}
	return false;
}

void Room::removeUser(LoggedUser lu) {
	std::vector<LoggedUser>::iterator vlui;
	for (vlui = this->m_users.begin(); vlui != this->m_users.end(); ++vlui) {
		if (vlui->getUsername() == lu.getUsername()) {
			this->m_users.erase(vlui);
			break;
		}
	}
}

std::vector<std::string> Room::getAllUsers() {
	std::vector<std::string> vs;
	int i = 0;
	for (i = 0; i < this->m_users.size(); i++) {
		vs.push_back(this->m_users[i].getUsername());
	}
	return vs;
}

RoomData Room::getData() {
	return this->m_metadata;
}

std::vector<SOCKET> Room::getAllUsersSockets() {
	int i = 0;
	std::vector<SOCKET> sockets;
	for (i = 0; i < this->m_users.size(); i++) {
		sockets.push_back(this->m_users[i].getClientSocket());
	}
	return sockets;
}

void Room::setActive() {
	this->m_metadata.isActive = 1;
}