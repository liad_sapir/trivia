#pragma once
#include <WinSock2.h>
#include <map>
#include <iostream>
#include <thread>
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"

class Communicator{
public:
    Communicator();
    ~Communicator();
    void startHandleRequests();
private:
    std::map<SOCKET, IRequestHandler*> m_clients;
    SOCKET m_serverSocket;
    RequestHandlerFactory m_handlerFactory;
    int m_serverPort;
    void bindAndListen();
    void handleNewClient(SOCKET client_sock);
    RequestInfo creatARequestInfoInstance(char* client_message, SOCKET client_sock);
};