#pragma once
#include <iostream>

class Question
{
public:
	Question(std::string q, std::string right, std::string op1, std::string op2, std::string op3);
	~Question();
private:
	std::string _question;
	std::string _right;
	std::string _option1;
	std::string _option2;
	std::string _option3;
};

