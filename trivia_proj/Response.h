#pragma once

#include "RoomData.h"

typedef struct LoginResponse {
    unsigned int status;
}LoginResponse;

typedef struct SignUpResponse {
    unsigned int status;
}SignUpResponse;

typedef struct ErrorResponse {
    std::string message;
}ErrorResponse;

typedef struct LogoutResponse {
    unsigned int status;
}logoutResponse;

typedef struct GetRoomsResponse {
    unsigned int status;
    std::vector<RoomData> rooms;
}GetRoomsResponse;

typedef struct GetPlayersInRoomResponse {
    std::vector<std::string> players;
}GetPlayersInRoomResponse;

typedef struct GetHighScoreResponse {
    unsigned int status;
    std::vector<std::string> statistics;
}GetHighScoreResponse;

typedef struct GetPersonalStatsResponse {
    unsigned int status;
    std::vector<std::string> statistics;
}GetPersonalStatsResponse;

typedef struct JoinRoomResponse {
    unsigned int status;
}JoinRoomResponse;

typedef struct CreateRoomResponse {
    unsigned int status;
}CreateRoomResponse;

typedef struct CloseRoomResponse {
    unsigned int status;
}CloseRoomResponse;

typedef struct StartGameResponse {
    unsigned int status;
}StartGameResponse;

typedef struct GetRoomStateResponse {
    unsigned int status;
    std::vector<std::string> players;
    unsigned int questionCount;
    unsigned int answerTimeout;
    unsigned int hasGameBegun;
}GetRoomStateResponse;

typedef struct LeaveRoomResponse {
    unsigned int status;
}LeaveRoomResponse;