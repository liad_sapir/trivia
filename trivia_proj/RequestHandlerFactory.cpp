#include <iostream>
#include "RequestHandlerFactory.h"
#include "SqliteDatabase.h"

RequestHandlerFactory::RequestHandlerFactory() : m_database(new SqliteDatabase()) ,m_clientsHandlers(this->m_clientsHandlers){
	this->m_loginManager = LoginManager(this->m_database);
	this->m_StatisticsManager = StatisticsManager(this->m_database);
}

RequestHandlerFactory::~RequestHandlerFactory() {

}

void RequestHandlerFactory::setClientsMap(std::map<SOCKET, IRequestHandler*>* clientsMap) {
	this->m_clientsHandlers = clientsMap;
}

void RequestHandlerFactory::setClientHandler(SOCKET clientSock, IRequestHandler* newHandler) {
	std::map<SOCKET, IRequestHandler*>::iterator map_it = this->m_clientsHandlers->find(clientSock);
	if (map_it != this->m_clientsHandlers->end()) map_it->second = newHandler;
}

LoginManager& RequestHandlerFactory::getLoginManager() {
	return std::ref(this->m_loginManager);
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler() {
	return new LoginRequestHandler(std::ref(this->m_loginManager),std::ref(*this));
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser lu) {
	return new MenuRequestHandler(lu,std::ref(this->m_roomManager),std::ref(*this));
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager() {
	return std::ref(this->m_StatisticsManager);
}

RoomManager& RequestHandlerFactory::getRoomManager() {
	return std::ref(this->m_roomManager);
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(Room m_room, LoggedUser lu) {
	return new RoomAdminRequestHandler(m_room, lu, std::ref(this->m_roomManager),std::ref(*this));
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(Room m_room, LoggedUser lu) {
	return new RoomMemberRequestHandler(m_room, lu, std::ref(this->m_roomManager), std::ref(*this));
}

void RequestHandlerFactory::setDataBase(IDatabase* db) {
	this->m_database = db;
}