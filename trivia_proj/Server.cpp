#include "Server.h"
#include "SqliteDatabase.h"

Server::Server() {
	
}

Server::~Server(){
	this->m_communicator.~Communicator();
}

void Server::run(){
	this->m_communicator.startHandleRequests();
}