#include "SqliteDatabase.h"

#include "Question.h"

#define NAME "triviaDB.sqlite"
#define TABLES_CNT 3

#define Q_NUM 10

bool res = false;
std::list<Question> qs;
int res_int = 0;
float res_float = 0;
std::vector<std::string> v_str;

SqliteDatabase::SqliteDatabase() {
	int file_exist = _access(NAME, 0);
	int res = sqlite3_open(NAME, &this->_db);

	if (res != SQLITE_OK)
	{
		this->_db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
	}

	if (file_exist != 0)
	{
		// init database

		std::string sqlCreateStatements[TABLES_CNT] = { "CREATE TABLE USERS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, USER_NAME TEXT NOT NULL, PASSWORD TEXT NOT NULL, MAIL TEXT NOT NULL);",
														"CREATE TABLE QUESTIONS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, QUESTION TEXT NOT NULL, RIGHT TEXT NOT NULL, OPTION1 TEXT NOT NULL, OPTION2 TEXT NOT NULL, OPTION3 TEXT NOT NULL);",
														"CREATE TABLE STATISTICS(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, USER_ID INTEGER NOT NULL, QUESTION_ID INTEGER NOT NULL, ANSWER_TIME FLOAT, RIGHT INTEGER, FOREIGN KEY(USER_ID) REFERENCES USERS(ID), FOREIGN KEY(QUESTION_ID) REFERENCES QUESTIONS(ID));" };


		try
		{
			for (int i = 0; i < TABLES_CNT; i++) this->runSqlStatement(sqlCreateStatements[i], nullptr, nullptr, "could not init database");
			this->initializeQuestions();
		}
		catch (std::exception& e)
		{
			std::remove(NAME);
			std::cerr << e.what() << std::endl;
		}
	}
}

SqliteDatabase::~SqliteDatabase() {
	sqlite3_close(this->_db);
	this->_db = nullptr;
}

void SqliteDatabase::runSqlStatement(std::string sql_statement, int (*callback)(void* data, int argc, char** argv, char** azColName), void* data_output, std::string errOutput) {
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_db, sql_statement.c_str(), callback, data_output, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cerr << "Failed to run " << sql_statement << " in DB" << std::endl << errMessage << std::endl;
		throw MyException(errOutput);
	}
}

int SqliteDatabase::callback_check_if_got_results(void* data, int argc, char** argv, char** azColName)
{
	if (argc > 0) res = true;
	return 0;
}

bool SqliteDatabase::doesUserExist(std::string name) {
	res = false;
	this->runSqlStatement("SELECT * FROM USERS WHERE USER_NAME =\"" + name + "\"; ", SqliteDatabase::callback_check_if_got_results, nullptr, "could not check if user exists");
	return res;
}

bool SqliteDatabase::doesPasswordMatch(std::string name, std::string password) {
	res = false;
	this->runSqlStatement("SELECT * FROM USERS WHERE USER_NAME =\"" + name + "\" AND PASSWORD = \"" + password + "\"; ", SqliteDatabase::callback_check_if_got_results, nullptr, "could not check if password matches");
	return res;
}

void SqliteDatabase::addNewUser(std::string name, std::string password, std::string mail) {
	std::string query = "INSERT INTO USERS(USER_NAME, PASSWORD, MAIL) VALUES(";
	query += '"' + name + '"' + ',' + '"';
	query += password + '"' + ',' + '"';
	query += mail + '"' + ");";
	this->runSqlStatement(query, nullptr, nullptr, "could not add user to database");
}

int SqliteDatabase::callback_get_questions(void* data, int argc, char** argv, char** azCalName) {
	std::string q, r, op1, op2, op3;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azCalName[i]) == "QUESTION") q = argv[i];
		else if (std::string(azCalName[i]) == "RIGHT") r = argv[i];
		else if (std::string(azCalName[i]) == "OPTION1") op1 = argv[i];
		else if (std::string(azCalName[i]) == "OPTION2") op2 = argv[i];
		else if (std::string(azCalName[i]) == "OPTION3") op3 = argv[i];
	}
	qs.push_back(Question(q, r, op1, op2, op3));
	return 0;
}

std::list<Question> SqliteDatabase::getQuestions() {
	qs.clear();
	this->runSqlStatement("SELECT * FROM QUESTIONS;", SqliteDatabase::callback_get_questions, nullptr, "could not get questions");
	return qs;
}

void SqliteDatabase::initializeQuestions() {
	std::string sqlCreateStatements[Q_NUM] = { "' ', ' ', ' ', ' ', ' '",
												"' ', ' ', ' ', ' ', ' '",
												"' ', ' ', ' ', ' ', ' '",
												"' ', ' ', ' ', ' ', ' '",
												"' ', ' ', ' ', ' ', ' '",
												"' ', ' ', ' ', ' ', ' '",
												"' ', ' ', ' ', ' ', ' '",
												"' ', ' ', ' ', ' ', ' '",
												"' ', ' ', ' ', ' ', ' '",
												"' ', ' ', ' ', ' ', ' '" };
	for (int i = 0; i < Q_NUM; i++) this->runSqlStatement("INSERT INTO QUESTIONS(QUESTION, RIGHT, OPTION1, OPTION2, OPTION3) VALUES(" + sqlCreateStatements[i] + ");", nullptr, nullptr, "could not init questions");
}

int SqliteDatabase::getUserId(std::string name) {
	this->runSqlStatement("SELECT * FROM USERS WHERE USER_NAME = \"" + name + "\";", callback_get_int_result, nullptr, "could not get user id");
	return res_int;
}

int SqliteDatabase::callback_get_float_result(void* data, int argc, char** argv, char** azCalName) {
	if (argc > 1) res_float = std::stof(argv[0]);
	return 0;
}

int SqliteDatabase::callback_get_int_result(void* data, int argc, char** argv, char** azCalName) {
	if (argc > 1) res_int = std::atoi(argv[0]);
	return 0;
}

float SqliteDatabase::getPlayerAverageAnswerTime(std::string name) {
	this->runSqlStatement("SELECT AVG(ANSWER_TIME) FROM STATISTICS WHERE USER_ID = " + std::to_string(this->getUserId(name)) + ';', callback_get_float_result, nullptr, "could not get player average answer time");
	return res_float;
}

int SqliteDatabase::getNumOfCorrectAnswers(std::string name) {
	this->runSqlStatement("SELECT SUM(RIGHT) AS CNT_RIGHT FROM STATISTICS WHERE USER_ID = " + std::to_string(this->getUserId(name)) + ';', callback_get_int_result, nullptr, "could not get number of correct answers");
	return res_int;
}

int SqliteDatabase::getNumOfTotalAnswers(std::string name) {
	this->runSqlStatement("SELECT COUNT(USER_ID) AS CNT FROM STATISTICS WHERE USER_ID = " + std::to_string(this->getUserId(name)) + " AND ANSWER_TIME IS NOT NULL;", callback_get_int_result, nullptr, "could not get number of total answers");
	return res_int;
}

int SqliteDatabase::getNumOfPlayerGames(std::string name) {
	this->runSqlStatement("SELECT COUNT(USER_ID) AS CNT FROM STATISTICS WHERE USER_ID = " + std::to_string(this->getUserId(name)) + ";", callback_get_int_result, nullptr, "could not get number of player games");
	return res_int;
}

int SqliteDatabase::callback_get_users(void* data, int argc, char** argv, char** azCalName) {
	v_str.push_back(argv[0]);
	return 0;
}

std::vector<std::string> SqliteDatabase::getLoggedUsers() {
	v_str.clear();
	this->runSqlStatement("SELECT USER_NAME FROM USERS;", callback_get_users, nullptr, "could not get users");
	return v_str;
}