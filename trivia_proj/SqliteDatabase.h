#pragma once

#include "sqlite3.h"

#include <iostream>
#include <io.h>
#include <list>
#include <vector>

#include "IDatabase.h"
#include "MyExeption.h"

class SqliteDatabase : public IDatabase
{
public:
	SqliteDatabase();
	~SqliteDatabase();
	virtual bool doesUserExist(std::string name);
	virtual bool doesPasswordMatch(std::string name, std::string password);
	virtual void addNewUser(std::string name, std::string password, std::string mail);

	virtual std::list<Question> getQuestions();

	virtual float getPlayerAverageAnswerTime(std::string name);
	virtual int getNumOfCorrectAnswers(std::string name);
	virtual int getNumOfTotalAnswers(std::string name);
	virtual int getNumOfPlayerGames(std::string name);

	virtual std::vector<std::string> getLoggedUsers();

private:
	sqlite3* _db;

	//helpers
	void initializeQuestions();

	void runSqlStatement(std::string sql_statement, int (*callback)(void* data, int argc, char** argv, char** azColName), void* data_output, std::string errOutput);

	int getUserId(std::string name);

	//callbacks
	static int callback_check_if_got_results(void* data, int argc, char** argv, char** azColName);
	static int callback_get_questions(void* data, int argc, char** argv, char** azCalName);
	static int callback_get_float_result(void* data, int argc, char** argv, char** azCalName);
	static int callback_get_int_result(void* data, int argc, char** argv, char** azCalName);
	static int callback_get_users(void* data, int argc, char** argv, char** azCalName);
};

