#pragma once
#include "RoomAdminRequestHandler.h"
#include "Response.h"
#include "jsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "Helper.h"

RoomAdminRequestHandler::RoomAdminRequestHandler(Room room, LoggedUser user, RoomManager& roomManager, RequestHandlerFactory& rhf) :m_handlerHelper(new RoomMemberRequestHandler(room, user, roomManager, rhf)), m_user(user), m_handlerFactory(rhf), m_room(room), m_roomManager(roomManager) {

}

RoomAdminRequestHandler::~RoomAdminRequestHandler(){
	
}

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo ri) {
	unsigned char id = ri.buffer[0];
	return id == CLOSE_ROOM_CODE || id == START_GAME_CODE || id == GET_ROOM_STATE_CODE;
}

RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo ri) {
	unsigned char id = ri.buffer[0];
	RequestResult rr;
	ErrorResponse er = { "not right handler changing now!" };
	if (this->isRequestRelevant(ri)) {
		switch (id)
		{
		case CLOSE_ROOM_CODE:
			rr = this->closeRoom(ri);
			break;
		case START_GAME_CODE:
			rr = this->startGame(ri);
			break;
		case GET_ROOM_STATE_CODE:
			rr = this->m_handlerHelper->getRoomState(ri, true);
			break;
		default:
			break;
		}
	}
	else {
		rr.buffer = JsonResponsePacketSerializer::serializeResponse(er);
		rr.newHandler = nullptr;
		this->logoutPlayer(ri);
	}
	return rr;
}

RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo ri) {
	RequestResult rr;
	CloseRoomResponse crr = { 1 };
	rr.buffer = JsonResponsePacketSerializer::serializeResponse(crr);
	this->changeUsersHandlers(ri.client_sock, this->m_room.getAllUsersSockets(), this->m_room.getAllUsers(), CLOSE_GAME);
	this->m_roomManager.deleteRoom(this->m_room.getData().name);
	rr.newHandler = this->m_handlerFactory.createMenuRequestHandler(this->m_user);
	return rr;
}

RequestResult RoomAdminRequestHandler::startGame(RequestInfo ri) {
	RequestResult rr;
	StartGameResponse sgr = { 1 };
	this->changeUsersHandlers(ri.client_sock, this->m_room.getAllUsersSockets(), this->m_room.getAllUsers(), START_GAME);
	this->m_room.setActive();
	this->m_roomManager.updateRoom(this->m_room);
	rr.buffer = JsonResponsePacketSerializer::serializeResponse(sgr);
	rr.newHandler = nullptr;
	this->logoutPlayer(ri);
	return rr;
}

void RoomAdminRequestHandler::changeUsersHandlers(SOCKET mainSocket, std::vector<SOCKET> usersSockets, std::vector<std::string> usersNames, int type) {
	int i = 0;
	LeaveRoomResponse lrr = { 1 };
	StartGameResponse sgr = { 1 };
	std::vector<unsigned char> lrrm = JsonResponsePacketSerializer::serializeResponse(lrr);
	std::vector<unsigned char> sgrm = JsonResponsePacketSerializer::serializeResponse(sgr);
	switch (type) {
	case CLOSE_GAME:
		for (i = 0; i < usersSockets.size(); i++) {
			if (mainSocket != usersSockets[i]) {
				this->m_handlerFactory.setClientHandler(usersSockets[i], this->m_handlerFactory.createMenuRequestHandler(LoggedUser(usersNames[i], usersSockets[i])));
			}
		}
		break;
	case START_GAME:
		for (i = 0; i < usersSockets.size(); i++) {
			if (mainSocket != usersSockets[i]) {
				this->m_handlerFactory.setClientHandler(usersSockets[i], nullptr);
			}
		}
		break;
	}
}

void RoomAdminRequestHandler::logoutPlayer(RequestInfo ri) {
	this->m_roomManager.deleteRoom(this->m_room.getData().name);
	this->changeUsersHandlers(ri.client_sock, this->m_room.getAllUsersSockets(), this->m_room.getAllUsers(), CLOSE_GAME);
	this->m_handlerFactory.getLoginManager().logout(this->m_user.getUsername());
}

std::string RoomAdminRequestHandler::getTypeOfHandler() {
	return "admin handler";
}