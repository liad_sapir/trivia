#include "LoginRequestHandler.h"
#include <iostream>

#include "jsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "Helper.h"
#include "Request.h"
//#include "Response.h"
#include "ids.h"

#include "LoginManager.h"
#include "RequestHandlerFactory.h"

LoginRequestHandler::LoginRequestHandler(LoginManager& lm, RequestHandlerFactory& rhf) : m_loginManager(lm), m_handlerFactory(rhf){

}

LoginRequestHandler::~LoginRequestHandler() {

}

bool LoginRequestHandler::isRequestRelevant(RequestInfo ri) {
	return (this->checkReqType(ri) == LOGIN_CODE || this->checkReqType(ri) == SIGNUP_CODE);
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo ri) {
	ErrorResponse er;
	RequestResult rr;
	if (this->isRequestRelevant(ri)) {
		if (this->checkReqType(ri) == LOGIN_CODE) rr = this->login(ri);
		else rr = this->signUp(ri);
	}
	else {
		er.message = "illegal request";
		rr.buffer = JsonResponsePacketSerializer::serializeResponse(er);
		rr.newHandler = this;
	}
	return rr;
}

RequestResult LoginRequestHandler::login(RequestInfo ri) {
	RequestResult rr;
	LoginResponse lrs;
	LoggedUser lu("default", NULL);
	LoginRequest lrq = JsonRequestPacketDeserializer::deserializeLoginRequest(ri.buffer);
	lu = this->m_loginManager.login(lrq.username, lrq.password, ri.client_sock);
	lrs.status = lu.getUsername() == " " ? 0 : 1;
	rr.buffer = JsonResponsePacketSerializer::serializeResponse(lrs);
	if (!lrs.status) rr.newHandler = this;
	else rr.newHandler = this->m_handlerFactory.createMenuRequestHandler(lu);
	return rr;
}

int LoginRequestHandler::checkReqType(RequestInfo ri) {
	if (ri.buffer.size()) return ri.buffer[0];
	return 0;
}

RequestResult LoginRequestHandler::signUp(RequestInfo ri) {
	RequestResult rr;
	SignUpResponse srs;
	SignUpRequest srq = JsonRequestPacketDeserializer::deserializeSignUpRequest(ri.buffer);
	if (this->m_loginManager.signUp(srq.username, srq.password, srq.email)) srs.status = 1;
	else srs.status = 0;
	rr.buffer = JsonResponsePacketSerializer::serializeResponse(srs);
	rr.newHandler = this;
	return rr;
}

std::string LoginRequestHandler::getTypeOfHandler() {
	return "login handler";
}