#ifndef ROOMMANAGER_H
#define ROOMMANAGER_H

#include <map>

#include "Room.h"

class RoomManager{
public:
	RoomManager();
	~RoomManager();
	bool createRoom(LoggedUser lu, RoomData rd);
	void deleteRoom(std::string name);
	unsigned int getRoomState(unsigned int id);
	Room getRoomByName(std::string name);
	std::vector<RoomData> getRooms();
	void updateRoom(Room room);

private:
	std::map<unsigned int, Room> m_rooms;

};

#endif // !ROOMMANAGER_H