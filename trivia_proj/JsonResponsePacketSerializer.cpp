#include "jsonResponsePacketSerializer.h"
#include "Helper.h"

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(ErrorResponse res){
    std::string message_string = "{message: \"" + res.message + "\"}";
    std::vector<unsigned char> result_vector;
    Helper::updateResultVector(std::ref(result_vector),message_string,ERROR_CODE);
    return result_vector;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LoginResponse res){
    return Helper::onlyStatusRes(res.status, LOGIN_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(SignUpResponse res){
    return Helper::onlyStatusRes(res.status, SIGNUP_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LogoutResponse res) {
    return Helper::onlyStatusRes(res.status, LOGOUT_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse res) {
    std::string message_string = "{Rooms: ";
    message_string += '"';
    std::vector<unsigned char> result_vector;
    int i = 0;
    for (i = 0; i < res.rooms.size(); i++) {
        message_string += res.rooms[i].name + ',';
    }
    if (res.rooms.size()) message_string.erase(std::prev(message_string.end()));
    message_string += "\", status: " + std::to_string(res.status) + '}';
    Helper::updateResultVector(std::ref(result_vector), message_string, GET_ROOMS_CODE);
    return result_vector;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse res) {
    std::string message_string = "{PlayersInRoom: ";
    message_string += '"';
    std::vector<unsigned char> result_vector;
    int i = 0;
    for (i = 0; i < res.players.size(); i++) {
        message_string += res.players[i] + ',';
    }
    if (res.players.size()) message_string.erase(std::prev(message_string.end()));
    message_string += '"';
    message_string += '}';
    Helper::updateResultVector(std::ref(result_vector), message_string, GET_PLAYERS_IN_ROOM_CODE);
    return result_vector;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse res) {
    return Helper::onlyStatusRes(res.status, JOIN_ROOM_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse res) {
    return Helper::onlyStatusRes(res.status, CREATE_ROOM_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetPersonalStatsResponse res) {
    std::string message_string = "{UserStatistics: ";
    message_string += '"';
    std::vector<unsigned char> result_vector;
    int i = 0;
    for (i = 0; i < res.statistics.size(); i++) {
        message_string += res.statistics[i] + ',';
    }
    message_string.erase(std::prev(message_string.end()));
    message_string += '"';
    message_string += '}';

    Helper::updateResultVector(std::ref(result_vector), message_string, GET_PERSONAL_STATS_CODE);
    return result_vector;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetHighScoreResponse res) {
    std::string message_string = "{HighScores: \"";
    message_string += '"';
    std::vector<unsigned char> result_vector;
    int i = 0;
    for (i = 0; i < res.statistics.size(); i++) {
        message_string += res.statistics[i] + ',';
    }
    message_string.erase(std::prev(message_string.end()));
    message_string += "\"}";
    Helper::updateResultVector(std::ref(result_vector), message_string, GET_HIGH_SCORES_CODE);
    return result_vector;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse res) {
    return Helper::onlyStatusRes(res.status, CLOSE_ROOM_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(StartGameResponse res) {
    return Helper::onlyStatusRes(res.status, START_GAME_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse res) {
    std::string message_string = "{status: " + std::to_string(res.status) + ",\"";
    std::vector<unsigned char> result_vector;
    int i = 0;
    for (i = 0; i < res.players.size(); i++) message_string += res.players[i] + ',';
    message_string.erase(std::prev(message_string.end()));
    message_string += "\", answerCount:" + std::to_string(res.questionCount) + ", answerTimeout:" + std::to_string(res.answerTimeout) + ", hasGameBegun:" + std::to_string(res.hasGameBegun) + '}';
    Helper::updateResultVector(std::ref(result_vector), message_string, GET_ROOM_STATE_CODE);
    return result_vector;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse res) {
    return Helper::onlyStatusRes(res.status, LEAVE_ROOM_CODE);
}