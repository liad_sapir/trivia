#define _CRT_SECURE_NO_WARNINGS

#include "Communicator.h"
#include "Helper.h"
#include "Response.h"
#include "RequestHandlerFactory.h"

#include <ctime>

int new_message_id = 1;
int user_id = 1;

Communicator::Communicator(){
	this->m_serverPort = 7777;
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	this->m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (this->m_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");

	this->m_handlerFactory.setClientsMap(&this->m_clients);
}

Communicator::~Communicator() {
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(this->m_serverSocket);
	}
	catch (...) {}
}

void Communicator::bindAndListen() {
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(this->m_serverPort); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(this->m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(this->m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << this->m_serverPort << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		// notice that we step out to the global namespace
		// for the resolution of the function accept

		// this accepts the client and create a specific socket from server to this client
		SOCKET client_socket = ::accept(this->m_serverSocket, NULL, NULL);

		if (client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__);

		std::cout << "Client accepted. Server and client can speak" << std::endl;

		this->m_clients.insert(std::pair<SOCKET, IRequestHandler*>(client_socket, new LoginRequestHandler(this->m_handlerFactory.getLoginManager(), std::ref(this->m_handlerFactory))));
		// the function that handle the conversation with the client
		std::thread client_sock(&Communicator::handleNewClient, this, client_socket);
		client_sock.detach();
	}
}

void Communicator::startHandleRequests() {
	this->bindAndListen();
}

void Communicator::handleNewClient(SOCKET client_sock) {
	char client_message[DEFAULT_SIZE] = { 0 };
	RequestResult rr;
	RequestInfo ri;
	int current_user_id = user_id++;
	this->m_clients[client_sock] = this->m_handlerFactory.createLoginRequestHandler();
	try {
		while (this->m_clients[client_sock] != nullptr && Helper::is_sock_alive(client_sock)) {
			recv(client_sock, client_message, DEFAULT_SIZE, 0);
			ri = Communicator::creatARequestInfoInstance(client_message, client_sock);
			std::cout << "client " << current_user_id << " sent: " << client_message << std::endl;
			if(this->m_clients[client_sock] != nullptr) rr = this->m_clients[client_sock]->handleRequest(ri); //added if because member handler can be changed to nullptr mid handeling when admin pressing start
			this->m_clients[client_sock] = rr.newHandler;
			send(client_sock, Helper::to_char(rr.buffer), rr.buffer.size() + 1, 0);
			std::cout << "returned: ";
			for (int i = 0; i < rr.buffer.size(); i++) std::cout << rr.buffer[i];
			std::cout << std::endl;
			Helper::eraseMessageData(client_message);
		}
		if (this->m_clients[client_sock] != nullptr && this->m_clients[client_sock]->getTypeOfHandler() != "login handler") this->m_clients[client_sock]->logoutPlayer(ri);
	}
	catch (std::exception& e) {
		std::cout << e.what() << std::endl; 
		//if (this->m_clients[client_sock] != nullptr) this->m_handlerFactory.getRoomManager().logoutUser(client_sock);
	}
}

RequestInfo Communicator::creatARequestInfoInstance(char* client_message, SOCKET client_sock) {
	RequestInfo ri;
	time_t now = time(0);
	ri.buffer = Helper::to_vector(client_message);
	ri.id = new_message_id++;
	ri.receivalTime = std::ctime(&now);
	ri.client_sock = client_sock;
	return ri;
}