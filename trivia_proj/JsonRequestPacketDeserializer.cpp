#include "JsonRequestPacketDeserializer.h"
#include "Helper.h"

jsoncons::json data;

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(std::vector<unsigned char> buffer) {
	LoginRequest finalReq;
	data = Helper::extractData(buffer);
	finalReq.username = data["username"].as<std::string>();
	finalReq.password = data["password"].as<std::string>();
	return finalReq;
}

SignUpRequest JsonRequestPacketDeserializer::deserializeSignUpRequest(std::vector<unsigned char> buffer) {
	SignUpRequest finalReq;
	data = Helper::extractData(buffer);
	finalReq.username = data["username"].as<std::string>();
	finalReq.password = data["password"].as<std::string>();
	finalReq.email = data["email"].as<std::string>();
	return finalReq;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(std::vector<unsigned char> buffer) {
	GetPlayersInRoomRequest gprr;
	data = Helper::extractData(buffer);
	gprr.roomName = data["roomName"].as<std::string>();
	return gprr;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(std::vector<unsigned char> buffer) {
	JoinRoomRequest jrr;
	data = Helper::extractData(buffer);
	jrr.roomName = data["roomName"].as<std::string>();
	return jrr;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(std::vector<unsigned char> buffer) {
	CreateRoomRequest crr;
	data = Helper::extractData(buffer);
	crr.roomName = data["roomName"].as<std::string>();
	crr.maxUsers = data["maxUser"].as<unsigned int>();
	crr.questionCount = data["questionCount"].as<unsigned int>();
	crr.answerTimeout = data["answerTimeout"].as<unsigned int>();
	return crr;
}