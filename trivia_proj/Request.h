#pragma once

typedef struct LoginRequest {
	std::string username;
	std::string password;
}LoginRequest;

typedef struct SignUpRequest {
	std::string username;
	std::string password;
	std::string email;
}SignUpRequest;

typedef struct GetPlayersInRoomRequest {
	std::string roomName;
}GetPlayersInRoomRequest;

typedef struct JoinRoomRequest {
	std::string roomName;
}JoinRoomRequest;

typedef struct CreateRoomRequest {
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
}CreateRoomRequest;