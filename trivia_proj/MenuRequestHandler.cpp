#include "MenuRequestHandler.h"
#include "Response.h"
#include "jsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"

static unsigned int room_id = 1;

MenuRequestHandler::MenuRequestHandler(LoggedUser user, RoomManager& rm, RequestHandlerFactory& rhf): m_user(user), m_roomManager(rm), m_handlerFactory(rhf){
}

MenuRequestHandler::~MenuRequestHandler() {

}

bool MenuRequestHandler::isRequestRelevant(RequestInfo ri) {
	unsigned char id = ri.buffer[ID_INDEX];
	if (id == LOGOUT_CODE || id == GET_ROOMS_CODE || id == GET_PLAYERS_IN_ROOM_CODE || id == JOIN_ROOM_CODE || id == GET_PERSONAL_STATS_CODE || id == GET_HIGH_SCORES_CODE || id == CREATE_ROOM_CODE) return true;
	return false;
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo ri) {
	ErrorResponse er = {"probably an syncronized! syncing now!"};
	RequestResult rr;
	unsigned char id = ri.buffer[ID_INDEX];
	if (this->isRequestRelevant(ri)) {
		switch (id) {
		case LOGOUT_CODE:
			rr = this->signout(ri);
			break;
		case GET_ROOMS_CODE:
			rr = this->getRooms(ri);
			break;
		case GET_PLAYERS_IN_ROOM_CODE:
			rr = this->getPlayersInRoom(ri);
			break;
		case JOIN_ROOM_CODE:
			rr = this->joinRoom(ri);
			break;
		case CREATE_ROOM_CODE:
			rr = this->createRoom(ri);
			break;
		case GET_PERSONAL_STATS_CODE:
			rr = this->getStatistics(ri);
			break;
		case GET_HIGH_SCORES_CODE:
			rr = this->getHighScores(ri);
			break;
		}
	}
	else {
		rr.buffer = JsonResponsePacketSerializer::serializeResponse(er);
		rr.newHandler = this;
	}
	return rr;
}

RequestResult MenuRequestHandler::signout(RequestInfo ri) {
	RequestResult rr;
	LogoutResponse lr;
	lr.status = this->m_handlerFactory.getLoginManager().logout(this->m_user.getUsername()) ? 1 : 0;
	rr.buffer = JsonResponsePacketSerializer::serializeResponse(lr);
	rr.newHandler = this->m_handlerFactory.createLoginRequestHandler();
	return rr;
}

RequestResult MenuRequestHandler::getRooms(RequestInfo ri) {
	RequestResult rr;
	GetRoomsResponse grr;
	grr.rooms = this->m_roomManager.getRooms();
	grr.status = grr.rooms.size() > 0 ? 1 : 0;
	rr.buffer = JsonResponsePacketSerializer::serializeResponse(grr);
	rr.newHandler = this;
	return rr;
}

RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo ri) {
	GetPlayersInRoomResponse gpirs;
	GetPlayersInRoomRequest gpirq;
	RequestResult rr;
	gpirq = JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(ri.buffer);
	gpirs.players = this->m_roomManager.getRoomByName(gpirq.roomName).getAllUsers();
	rr.buffer = JsonResponsePacketSerializer::serializeResponse(gpirs);
	rr.newHandler = this;
	return rr;
}

RequestResult MenuRequestHandler::joinRoom(RequestInfo ri) {
	JoinRoomRequest jrq;
	JoinRoomResponse jrs;
	Room roomToJoin;
	RequestResult rr;
	jrq = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(ri.buffer);
	roomToJoin = this->m_roomManager.getRoomByName(jrq.roomName);
	jrs.status = roomToJoin.addUser(this->m_user) ? 1 : 0;
	this->m_roomManager.updateRoom(roomToJoin);
	rr.buffer = JsonResponsePacketSerializer::serializeResponse(jrs);
	rr.newHandler = this->m_handlerFactory.createRoomMemberRequestHandler(roomToJoin,m_user);
	return rr;
}

RequestResult MenuRequestHandler::createRoom(RequestInfo ri) {
	CreateRoomRequest crq;
	CreateRoomResponse crs;
	RoomData rd;
	RequestResult rr;
	crq = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(ri.buffer);
	rd.id = room_id++;
	rd.isActive = 0;
	rd.maxPlayers = crq.maxUsers;
	rd.name = crq.roomName;
	rd.numOfQuestionsInGame = crq.questionCount;
	rd.timePerQuestion = crq.answerTimeout;
	crs.status = this->m_roomManager.createRoom(this->m_user, rd) ? 1 : 0;
	rr.buffer = JsonResponsePacketSerializer::serializeResponse(crs);
	if (crs.status) rr.newHandler = this->m_handlerFactory.createRoomAdminRequestHandler(this->m_roomManager.getRoomByName(rd.name), this->m_user);
	else rr.newHandler = this;
	return rr;
}

RequestResult MenuRequestHandler::getStatistics(RequestInfo ri) {
	RequestResult rr;
	GetPersonalStatsResponse gpsr;
	gpsr.statistics = this->m_handlerFactory.getStatisticsManager().getUserStatistics(this->m_user.getUsername());
	gpsr.status = gpsr.statistics.size() > 0 ? 1 : 0;
	rr.buffer = JsonResponsePacketSerializer::serializeResponse(gpsr);
	rr.newHandler = this;
	return rr;
}

RequestResult MenuRequestHandler::getHighScores(RequestInfo ri) {
	RequestResult rr;
	GetHighScoreResponse gsr;
	gsr.statistics = this->m_handlerFactory.getStatisticsManager().getHighScore();
	gsr.status = gsr.statistics.size() > 0 ? 1 : 0;
	rr.buffer = JsonResponsePacketSerializer::serializeResponse(gsr);
	rr.newHandler = this;
	return rr;
}

void MenuRequestHandler::logoutPlayer(RequestInfo ri) {
	this->m_handlerFactory.getLoginManager().logout(this->m_user.getUsername());
}

std::string MenuRequestHandler::getTypeOfHandler() {
	return "menu handler";
}